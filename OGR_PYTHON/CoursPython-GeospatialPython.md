# Manipulations spatiales et attributaires avec OGR en Python

![](https://framagit.org/Guilhain/manuels/-/raw/1357f186846cc59b1643c9652f1c70f34c9d1005/OGR_PYTHON/img/logo_250x250.png)


Rédigé par Guilhain Averlant en mars 2013, dernière révision novembre 2024, 5ème édition.
Licence : Licence Creative Commons de type [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0) v. 2024-11.


> L'esprit devenu capable de fonctionner
> comme une machine se reconnaît dans
> la machine capable de fonctionner com-
> me lui — sans s'apercevoir qu'en vérité 
> la machine ne fonctionne pas comme 
> l'esprit, mais seulement comme l'esprit
> ayant appris à fonctionner comme une 
> machine.
>
> -- <cite>André Gorz</cite>

## Note de l’auteur

### Pour qui ? Pourquoi ?

Le présent manuel est le support de cours que je dispense depuis novembre 2012 sur la librairie OGR / GDAL au Master 2 Carthagéo — Université Paris 1 / Paris 7 et ENSG. Idéalement afin de pouvoir en tirer les enseignements dans leur intégralité il faudrait idéalement y consacrer au moins 24 heures d’enseignement. 
Cet ouvrage doit être vu comme un document de support dont l’idée principale est d’apprendre les bases d’OGR afin d’aller plus loin par la suite.
Même si les étudiants sont la première cible de ce manuel, il est plus largement destiné à l'ensemble de ceux qui utilisent Python.

### L’idée fondatrice

A mes yeux cet ouvrage et les enseignements qu’il dispense s’inscrivent dans une démarche de conquête — voire reconquête — de la conception des outils informatiques en cartographie. En effet nous notons tous la prédominance de certains acteurs, propriétaires comme libre, dans les logiciels de géomatique. Cette prédominance est une preuve flagrante de l’intérêt que notre matière suscite auprès du secteur privé/public mais également pour le grand public.
J’observe pourtant un fort attachement, si ce n’est dépendance aux outils, au détriment de la connaissance, du savoir, qui a permis de les constituer. 
Ainsi l’idée fondatrice est de permettre à l’étudiant, mais aussi au cartographe, de repenser ce qu’il se passe derrière l’outil, comment celui-ci «masse» la donnée pour en sortir un résultat graphique ou statistique conforme aux attentes de l’utilisateur.

### Quelle licence ?

Après la rédaction, pour le moins fastidieuse, de ce manuel, s’est posée la question de sa licence de diffusion. Il fallait que sa distribution soit gratuite, et le reste, mais aussi que les utilisateurs puissent le modifier et le partager à leur guise.
C’est donc tout naturellement que je me suis dirigé vers le dispositif de licence Creative Commons de type CC BY-SA.

## Table des matières
* [Note de l’auteur](#note-de-lauteur)
    * [Pour qui ? Pourquoi ?](#pour-qui-pourquoi-)
    * [L’idée fondatrice](#lidée-fondatrice)
    * [Quelle licence ?](#quelle-licence-)
* [Introduction](#introduction)
* [I - Installation](#i-installation)
    * [Windows](#windows)
    * [Mac OS](#mac-os)
    * [Linux](#linux)
* [II - L'information géospatiale : bref rappel](#ii-linformation-géospatiale-bref-rappel)
    * [De la donnée](#de-la-donnée)
    * [De la géométrie](#de-la-géométrie)
    * [Un format ](#un-format-)
* [III - La librairie GDAL/OGR](#iii-la-librairie-gdalogr)
    * [GDAL/OGR qu’est-ce ?](#gdalogr-quest-ce-)
    * [les formats vectoriels](#les-formats-vectoriels)
* [IV - Manipulation de base](#iv-manipulation-de-base)
    * [Appeler la libraire OGR](#appeler-la-libraire-ogr)
    * [Ouvrir un fichier Shapefile](#ouvrir-un-fichier-shapefile)
    * [Ecrire un fichier au format Postgis](#ecrire-un-fichier-au-format-postgis)
    * [Supprimer un fichier SIG](#supprimer-un-fichier-sig)
    * [Récupérer une projection](#récupérer-une-projection)
    * [Atributer une projection](#atributer-une-projection)
    * [Réaliser une transformation de projection](#réaliser-une-transformation-de-projection)
    * [Les filtres attributaires](#les-filtres-attributaires)
    * [Exercice](#exercice)
* [V - Les opérateurs spatiaux](#v-les-opérateurs-spatiaux)
    * [Les testeurs spatiaux (booléen)](#les-testeurs-spatiaux-booléen)
    * [Les fonctions de géotraitement (agit sur la géométrie)](#les-fonctions-de-géotraitement-agit-sur-la-géométrie)
    * [Les filtres spatiaux](#les-filtres-spatiaux)
* [Exercice](#exercice)
    * [Partie I – Traitement préliminaire du fichier Hotspot Wifi](#partie-i-traitement-préliminaire-du-fichier-hotspot-wifi)
    * [Partie II – Réalisation des requêtes attributaires et spatiales](#partie-ii-réalisation-des-requêtes-attributaires-et-spatiales)
* [Conclusion](#conclusion)
* [Bibliographie non exhaustive](#bibliographie-non-exhaustive)

## Introduction

Vous avez désormais vu les rudiments du langage de programmation python. Désormais, nous allons voir comment ce langage peut être utilisé pour manipuler de l’information géospatiale afin de réaliser des outils d’exploitation des données et des géotraitements avec vos fichiers SIG.
	
Dans ce tutoriel introductif nous ne traiterons que des données vectorielles 2D mais il faut savoir que la librairie OGR/GDAL peut exploiter les données 2,5D.


## I - Installation

Autant le dire tout de suite, OGR/GDAL n'est pas une librairie très simple à installer, du moins quand on est pas utilisateur de linux.
Pour fonctionner, elle est dépendante d'un grand nombre de librairies. Toutefois, pas de panique, des utilisateurs bienveillants ont réalisé des packages prêts à l'emploi parfaitement satisfaisants pour une utilisation dans les formats les plus utilisés.

### Windows

Nous vous conseillons de suivre les étapes décritent par cet [article](https://opensourceoptions.com/blog/how-to-install-gdal-for-python-with-pip-on-windows/)

### Mac OS

Pour les utilisateurs de Mac OS nous conseillons de suivre les indications de cet [article](https://medium.com/@egiron/how-to-install-gdal-and-qgis-on-macos-catalina-ca690dca4f91)

### Linux

Pour les systèmes linux le plus simple est d'utiliser la commande suivante :

```shell
sudo apt-get install python-gdal
```

Autres infos disponibles sur le site d’Yves Jacolin dédiés à OGR / GDAL  : [http://gdal.gloobe.org/install.html](http://gdal.gloobe.org/install.html)

## II - L'information géospatiale : bref rappel

Généralement on a tendance à croire que l’information géographique forme un tout, à titre personnel je préfère opérer les distinctions suivantes :
* La donnée : contenue dans la table attributaire du fichier SIG. En fonction du typage des données qui s’y trouve on peut effectuer des opérations statistiques, des classifications des sélections en fonction d’attribut …
* La géométrie : elle se résume par un type (point, ligne, polygone…) représenté par une ou des coordonnées liées les unes aux autres. On peut évidemment les simplifier, les généraliser, les fusionner …
* Un format de stockage : cet aspect est quasi toujours oublié pourtant il a toute son importance, le format conditionne bien souvent l’espace disque de la donnée, mais aussi les typages des données et des géométries qu’il supporte et enfin son accessibilité au public.

### De la donnée

La donnée se caractérise par type une longueur et parfois d’une précision.

| Code OGR | Type de la donnée |
|----------|-------------------|
| 0        | Integer           |
| 1        | IntegerList       |
| 2        | Real              |
| 3        | RealList          |
| 4        | String            |
| 5        | StringList        |
| 6        | WideString        |
| 7        | WideStringList    |
| 8        | Binary            |
| 9        | Date              |
| 10       | DateTime          |

### De la géométrie

| Code OGR    | Type de la géométrie     |
|-------------|--------------------------|
| -2147483648 | wkb25Bit                 |
| -2147483641 | wkbGeometryCollection25D |
| -2147483642 | wkbMultiPolygon25D       |
| -2147483643 | wkbMultiLineString25D    |
| -2147483644 | wkbMultiPoint25D         |
| -2147483645 | wkbPolygon25D            |
| -2147483646 | wkbLineString25D         |
| -2147483647 | wkbPoint25D              |
| -2147483648 | wkb25DBit                |
| 0           | wkbUnknown               |
| 0           | wkbXDR                   |
| 1           | wkbNDR                   |
| 1           | wkbPoint                 |
| 100         | wkbNone                  |
| 101         | wkbLinearRing            |
| 2           | wkbLineString            |
| 3           | wkbPolygon               |
| 4           | wkbMultiPoint            |
| 5           | wkbMultiLineString       |
| 6           | wkbMultiPolygon          |
| 7           | wkbGeometryCollection    |


### Un format 

Même si la donnée et la géométrie sont ce qui caractérise le plus l’information d’un fichier SIG il ne faut pas négliger l’importance due au format dans lequel ces informations sont stockées. 
Le format conditionne à la fois le typage des données mais aussi les géométries. 
Par exemple concernant les données le format Shapefile tolère qu’on n’indique pas la précision des données pour des données de type double, contrairement au format Postgis qui forcera les données à apparaître en Integer si aucune précision n’est donnée. Pour la géométrie le format Shapefile tolère les self-intersection sur un polygone contrairement au format Postgis, de même le format Oracle Spatial refuse des polygones de moins de 4 coordonnées (des triangles).

## III - La librairie GDAL/OGR

La librairie GDAL/OGR est un projet open source, il faut bien être conscient que de plus en plus les projets open source en géomatique prennent de l’ampleur. Ceci au moins pour deux raisons : la première parce qu’ils arrivent à fédérer une grande communauté d’utilisateurs donc  une somme de compétences  et d’expertises vraiment considérable, ces projets sont donc techniquement très sérieux voire même plus fiables que des solutions propriétaires sur certains aspects, en particulier le support utilisateur … La seconde raison je l’attribue à des considérations financières : en effet pourquoi payer des solutions propriétaires chères si des solutions open source fonctionnent au moins aussi bien ?

Je tiens à focaliser votre attention sur cette circulaire de Jean Marc Ayrault datant du 19 septembre 2012 : 
[http://circulaire.legifrance.gouv.fr/pdf/2012/09/cir_35837.pdf]http://circulaire.legifrance.gouv.fr/pdf/2012/09/cir_35837.pdf

### GDAL/OGR qu’est-ce ?

Date de création : 1998
Créateur : Franck Wanderman
Licence : X11 / MIT
Un ensemble de librairies d’abord développées en C++ mais désormais disponibles pour 7 autres languages dont le Python. 

Ces librairies sont : 
* GDAL : traitement des images raster, spécifiquement utilisée pour la manipulation des images et la télédétection.
* OGR : traitement de fichiers vectoriels
* OSR : traitement des projections

### les formats vectoriels

Actuellement OGR/GDAL gère 81 formats vectoriels différents les plus utilises sont :

| Code OGR               | Format de fichier         |
|------------------------|---------------------------|
| ESRI Shapefile	        | ESRI Shapefile            |
| Geoconcept	            | Geoconcept                |
| Geojson                | Geojson                   |
| GPKG                   | GeoPackage    |
| GML                    | Geography Markup Language |
| KML	                   | Keyhole Markup Language   |
| MapInfo File	          | Map Info                  |
| Mysql                  | Mysql                     |
| OCI	                   |Oracle Spatial             |
| OpenFileGDB            |Geodatabase fichier        |
| PostgreSQL ou PostGIS	 | Postgres ou Postgis       |
| SDE	                   | ESRI SDE                  |

La liste des formats disponibles pour OGR est ici : 
[http://www.gdal.org/ogr/ogr_formats.html](http://www.gdal.org/ogr/ogr_formats.html)


## IV - Manipulation de base

Avant de commencer, il est nécessaire de savoir que l'entièreté des fonctions d'OGR est consultable ici : [http://gdal.org/python/](http://gdal.org/python/)

Les principales fonctions pour lire un fichier SIG peuvent être résumées par ce schéma ci-dessous :
![Schema : lecture avec OGR](https://framagit.org/Guilhain/manuels/-/raw/7029990fde6a2bc1b8adb104c8b4f2249f449696/OGR_PYTHON/img/OGR_Lecture.png)

### Appeler la libraire OGR

``` python
from osgeo import ogr
```

### Ouvrir un fichier Shapefile

``` python
from osgeo import ogr

# Création d'un driver en fonction du format du fichier (ici SHAPEFILE)
# Liste des noms de formats disponible ici : http://www.gdal.org/ogr/ogr_formats.html
driver = ogr.GetDriverByName("ESRI SHAPEFILE")

# Création d'une source de données pointant vers le Shapefile.
# Premier paramètre (string) : chemin vers la source
# Deuxième paramètre (booléen) : 1 si modifiable, 0 si en lecture seule
data_source = driver.Open("data/Polygon.shp", 0)

# Certains formats contiennent plusieurs couches d'information géographique,
# comme le MIF/MID ou les bases de données telles que PostGIS.
# Pour connaître le nombre de couches présentes, on utilise la fonction suivante :
print(f"Nombre de couches dans le fichier : {data_source.GetLayerCount()}")

# On ouvre la première (et unique) couche (par défaut, la valeur est zéro,
# il n'est donc pas nécessaire de la préciser dans ce cas).
layer = data_source.GetLayer(0)

# À partir de cette étape, le document est ouvert.
# On peut déjà récupérer des informations sur la couche :
print(f"Nom de la couche : {layer.GetName()}")
print(f"Enveloppe de la couche : {layer.GetExtent()}")
print(f"Nombre d'entités : {layer.GetFeatureCount()}")
print(f"Géométrie de la couche : {layer.GetGeomType()}")
print(f"Référence spatiale : {layer.GetSpatialRef()}")

# Pour accéder aux données de la couche, on doit pointer la définition de la couche.
layer_defn = layer.GetLayerDefn()
print(f"Nombre de champs : {layer_defn.GetFieldCount()}")

# Les informations sur les champs sont encapsulées dans la définition de la couche.
# On peut récupérer : le nom des champs, le type, la longueur, et la précision (si de type double).
for i in range(layer_defn.GetFieldCount()):
    print("\n")  # On saute une ligne pour plus de lisibilité
    print(f"Nom du champ {i} : {layer_defn.GetFieldDefn(i).GetName()}")
    print(f"Type du champ {i} : {layer_defn.GetFieldDefn(i).GetType()}")
    print(f"Longueur du champ {i} : {layer_defn.GetFieldDefn(i).GetWidth()}")
    print(f"Précision du champ {i} : {layer_defn.GetFieldDefn(i).GetPrecision()}")

# ACCÈS AUX DONNÉES ATTRIBUTAIRES

# ATTENTION : IL EXISTE DEUX MÉTHODES POUR ACCÉDER AUX DONNÉES

# MÉTHODE N°1
# Pour accéder à une entité par son numéro de ligne (attention à vérifier
# si le format prend en compte ou non les noms de champs comme appartenant à une ligne).
print("MÉTHODE 1 : j'affiche l'entité 1 (2ème ligne)")
feature = layer.GetFeature(1)
for i in range(layer_defn.GetFieldCount()):
    print(f"{layer_defn.GetFieldDefn(i).GetName()} : {feature.GetField(i)}")

print("\nMÉTHODE 2 : j'affiche toute la couche")
# MÉTHODE N°2
# Cette méthode permet de visualiser toutes les entités de la couche.
for feature in layer:
    for j in range(layer_defn.GetFieldCount()):
        print(f"{layer_defn.GetFieldDefn(j).GetName()} : {feature.GetField(j)}")

# ACCÈS À LA GÉOMÉTRIE
# La géométrie est également stockée dans l'objet feature.
# Nous allons examiner la 3ème entité de la couche.
feature = layer.GetFeature(2)
geometry = feature.GetGeometryRef()

# Récupération du type de géométrie
print(f"Type de géométrie : {geometry.GetGeometryName()}")
print(f"Code de géométrie OGR : {geometry.GetGeometryType()}")

# Puisque l'on travaille sur une couche de type polygone, on peut récupérer :
# - l'aire
print(f"Aire du polygone : {geometry.GetArea()}")
# - l'enveloppe
print(f"Enveloppe du polygone : {geometry.GetEnvelope()}")

# Les coordonnées en différents formats :
# - WKT (Well-Known Text)
print(f"Coordonnées en WKT : {geometry.ExportToWkt()}")
# - WKB (Well-Known Binary)
print(f"Coordonnées en WKB : {geometry.ExportToWkb()}")
# - GEOJSON
print(f"Coordonnées en GeoJSON : {geometry.ExportToJson()}")
```


### Ecrire un fichier au format Postgis

Voici un schéma résumant comment créer un fichier SIG :

![Schema  : écriture avec OGR](https://framagit.org/Guilhain/manuels/-/raw/7029990fde6a2bc1b8adb104c8b4f2249f449696/OGR_PYTHON/img/OGR_Ecriture.png)

``` python
from osgeo import ogr

## CREATION DE LA COUVERTURE
# Création d'un driver de connexion pour PostgreSQL
driver = ogr.GetDriverByName("POSTGRESQL")
# Définition du chemin de connexion pour la base de données PostgreSQL
data_source = driver.CreateDataSource(
    "PG: host='localhost' dbname='carthageo' user='postgres' password='postgres' port=5434"
)

# Création de la couche avec vérification préalable de son existence
if data_source.GetLayerByName("nouvelle_couche"):
    raise Exception(
        "La couche existe déjà, veuillez la supprimer dans la base avant de relancer le script."
    )
layer = data_source.CreateLayer("nouvelle_couche", None, 0)

## ECRITURE DES CHAMPS
# Création de champs dans la couche pour distinguer les différentes géométries :
# un identifiant (le code de géométrie), entier de longueur 3
# un nom (string) de longueur 20
# une aire (réel) de longueur 15 et précision 5
# un périmètre (réel) de longueur 15 et précision 5
field_name = ["ID", "NOM", "AIRE", "PERIMETRE"]
field_type = [0, 4, 2, 2]
field_width = [3, 20, 15, 15]
field_precision = [0, 0, 5, 5]

for i in range(len(field_name)):
    # Création de chaque champ avec son nom, type, longueur et précision
    field = ogr.FieldDefn(field_name[i], field_type[i])
    field.SetWidth(field_width[i])
    field.SetPrecision(field_precision[i])
    layer.CreateField(field)

## ECRITURE DES DONNEES ET DE LA GEOMETRIE
# Définition des géométries en WKT (Well-Known Text)
point = "POINT (30 10)"
ligne = "LINESTRING (30 10, 10 30, 40 40)"
polygone = "POLYGON ((35 10, 10 20, 15 40, 45 45, 35 10),(20 30, 35 35, 30 20, 20 30))"
multi_point = "MULTIPOINT ((10 40), (40 30), (20 20), (30 10))"
multi_ligne = "MULTILINESTRING ((10 10, 20 20, 10 40),(40 40, 30 30, 40 20, 30 10))"
multi_polygone = "MULTIPOLYGON (((40 40, 20 45, 45 30, 40 40)),((20 35, 45 20, 30 5, 10 10, 10 30, 20 35),(30 20, 20 25, 20 15, 30 20)))"

# Ajout de chaque géométrie individuellement, avec les informations spécifiques
# Il se peut que certains "warning d'OGR apparaissent sur le terminal python

# POINT
feature = ogr.Feature(layer.GetLayerDefn())
geometry = ogr.CreateGeometryFromWkt(point)
feature.SetGeometry(geometry)
feature.SetField("ID", 1)
feature.SetField("NOM", "Point")
feature.SetField("AIRE", geometry.Area())
feature.SetField("PERIMETRE", geometry.Length())
layer.CreateFeature(feature)

# LIGNE
feature = ogr.Feature(layer.GetLayerDefn())
geometry = ogr.CreateGeometryFromWkt(ligne)
feature.SetGeometry(geometry)
feature.SetField("ID", 2)
feature.SetField("NOM", "Ligne")
feature.SetField("AIRE", geometry.Area())
feature.SetField("PERIMETRE", geometry.Length())
layer.CreateFeature(feature)

# POLYGONE
feature = ogr.Feature(layer.GetLayerDefn())
geometry = ogr.CreateGeometryFromWkt(polygone)
feature.SetGeometry(geometry)
feature.SetField("ID", 3)
feature.SetField("NOM", "Polygone")
feature.SetField("AIRE", geometry.Area())
# Pour le champ périmètre sur les polygones, on doit extraire le contour
# puis calculer sa longueur
feature.SetField("PERIMETRE", geometry.Boundary().Length())
layer.CreateFeature(feature)

# MULTIPOINT
feature = ogr.Feature(layer.GetLayerDefn())
geometry = ogr.CreateGeometryFromWkt(multi_point)
feature.SetGeometry(geometry)
feature.SetField("ID", 4)
feature.SetField("NOM", "MultiPoint")
feature.SetField("AIRE", geometry.Area())
feature.SetField("PERIMETRE", geometry.Length())
layer.CreateFeature(feature)

# MULTILIGNE
feature = ogr.Feature(layer.GetLayerDefn())
geometry = ogr.CreateGeometryFromWkt(multi_ligne)
feature.SetGeometry(geometry)
feature.SetField("ID", 5)
feature.SetField("NOM", "MultiLigne")
feature.SetField("AIRE", geometry.Area())
feature.SetField("PERIMETRE", geometry.Length())
layer.CreateFeature(feature)

# MULTIPOLYGONE
feature = ogr.Feature(layer.GetLayerDefn())
geometry = ogr.CreateGeometryFromWkt(multi_polygone)
feature.SetGeometry(geometry)
feature.SetField("ID", 6)
feature.SetField("NOM", "MultiPolygon")
feature.SetField("AIRE", geometry.Area())
# Pour le champ périmètre sur les polygones, on doit extraire le contour
# puis calculer sa longueur
feature.SetField("PERIMETRE", geometry.Boundary().Length())
layer.CreateFeature(feature)
```


### Supprimer un fichier SIG

Certains formats, par exemple le format ESRI Shapefile pour ne pas le citer, peut comporter une multitude de fichiers, dans notre exemple à minima trois voir plus. Il peut donc paraitre fastidieux de les supprimer d'un répertoire. Heureusement OGR permet grâce à la fonction ```DeleteDataSource()```.

``` python
from osgeo import ogr

fichier = "data/del/Point.shp"
driver = ogr.GetDriverByName("ESRI Shapefile")

# Suppression du fichier Shapefile :
# Attention, cette opération ne peut être exécutée qu'une seule fois
# (le fichier doit exister la première fois que cette commande est exécutée).
driver.DeleteDataSource(fichier)
```

### Récupérer une projection

Une fois un fichier vecteur ouvert avec OGR une simple requête sur l’objet `layer.GetSpatialRef()` permet de récupérer le système de projection utilisée.

``` python
from osgeo import ogr

# Chargement du driver pour le format Shapefile
driver = ogr.GetDriverByName("ESRI Shapefile")

# Ouverture du fichier Shapefile en mode lecture seule (0)
data_source = driver.Open("data/Point.shp", 0)

# Récupération de la couche
layer = data_source.GetLayer()

# Affichage de la référence spatiale de la couche
print(f"Les référence spatial de la couche : {layer.GetSpatialRef()}")
```

Résultat

``` python
>>> GEOGCS["GCS_WGS_1984",
    DATUM["WGS_1984",
        SPHEROID["WGS_1984",6378137,298.257223563]],
    PRIMEM["Greenwich",0],
    UNIT["Degree",0.017453292519943295]]
```

### Atributer une projection

Lorsque l’on crée un fichier vectoriel, on a la possibilité d’indiquer une projection dans les paramètres de la fonction `CreateLayer()`.
Pour renseigner la projection que l’on veut choisir on va utiliser la librairie OSR.
Le site Spatial Reference permet de retrouver les codes de projections et ce sous différents formats, pour moi ce site fait référence : [http://spatialreference.org/](http://spatialreference.org/)

``` python
from osgeo import ogr
from osgeo import osr

# Création d'une variable de type SpatialReference pour stocker les références
# de projection géographique.
projection = osr.SpatialReference()

# Importation d'un système de projection en utilisant un code EPSG.
# On peut importer un système de projection via plusieurs formats ou sources,
# notamment WKT, PROJ4, URL, ESRI, EPSG, EPSGA, PCI, USGS, XML, et ERM.
projection.ImportFromEPSG(4326)

# Affichage de la projection pour vérification
print(projection)
```

Résultat :

``` python
>>> 
GEOGCS["GCS_WGS_1984",
    DATUM["WGS_1984",
        SPHEROID["WGS_1984",6378137,298.257223563]],
    PRIMEM["Greenwich",0],
    UNIT["Degree",0.017453292519943295]]
GEOGCS["WGS 84",
    DATUM["WGS_1984",
        SPHEROID["WGS 84",6378137,298.257223563,
            AUTHORITY["EPSG","7030"]],
        AUTHORITY["EPSG","6326"]],
    PRIMEM["Greenwich",0,
        AUTHORITY["EPSG","8901"]],
    UNIT["degree",0.0174532925199433,
        AUTHORITY["EPSG","9122"]],
    AUTHORITY["EPSG","4326"]]
```

### Réaliser une transformation de projection

``` python
from osgeo import ogr
from osgeo import osr

# Chargement du driver pour le format Shapefile
driver = ogr.GetDriverByName("ESRI Shapefile")

# Ouverture du fichier Shapefile en mode lecture seule (0)
data_source = driver.Open("data/Point.shp", 0)

# Récupération de la couche
layer = data_source.GetLayer()

# Création d'une instance de SpatialReference pour stocker la projection
projection = osr.SpatialReference()

# Importation d'un système de projection via un code EPSG.
# On peut importer un système de projection depuis différentes sources :
# WKT, PROJ4, URL, ESRI, EPSG, EPSGA, PCI, USGS, XML, et ERM.
projection.ImportFromEPSG(4326)
print(f"Référence de projection 4326 : {projection}")

# Définition des systèmes de référence pour la transformation de WGS84 (EPSG:4326) vers RGF93 (EPSG:2154)
WGS84 = osr.SpatialReference()
WGS84.ImportFromEPSG(4326)

RGF93 = osr.SpatialReference()
RGF93.ImportFromEPSG(2154)
print(f"Référence de projection 2154 : {RGF93}")
print()
# Coordonnées de Paris en WGS84
lat = 48.8155730
lon = 2.2241990
# Note : en WGS84, les coordonnées sont exprimées en (latitude, longitude), ce qui
# peut prêter à confusion car habituellement on utilise (x, y).

# Création d'une géométrie OGR pour représenter le point de Paris
paris = ogr.CreateGeometryFromWkt(f"POINT ({lon} {lat})")
print(f"Coordonnées de Paris en WGS84 : {paris}")

# Attribution du système de coordonnées WGS84 au point de Paris
paris.AssignSpatialReference(WGS84)

# Transformation des coordonnées du point de WGS84 vers RGF93
paris.TransformTo(RGF93)

# Affichage des coordonnées transformées en RGF93
print(f"Nouvelles coordonnées de Paris en RGF93 : {paris}")
```

Résultat:

``` python
>>> Nouvelles coordonnées de Paris en RGF93 : POINT (6965934.894937987439334 3096339.348330470267683)
```

### Les filtres attributaires

Parfois on a pas envie de travailler sur un jeu de données en entier et on a pas envie non plus de faire une boucle sur les attributs d'une couche. Pour ce faire on peut faire un filtre attributaire.
Le filtre attributaire se fait directement sur la variable layer via la fonction `SetAttributeFilter()`.

``` python
from osgeo import ogr

# Chemin du fichier Shapefile
fichier = "data/countries/countries.shp"

# Chargement du driver pour le format Shapefile
driver = ogr.GetDriverByName("ESRI Shapefile")

# Ouverture du fichier Shapefile
data_source = driver.Open(fichier)
layer = data_source.GetLayer()

# Application d'un filtre attributaire pour sélectionner les pays dont le champ
# "continent" est défini comme étant "Africa"
layer.SetAttributeFilter("continent = 'Africa'")

# Affichage du nom des pays dans le résultat de la requête
# Le nom du pays est stocké dans le champ "sovereignt"
for feature in layer:
    print(feature.GetField("sovereignt"))
```

### Exercice

Créer un outil de conversion de format de fichier vectoriel.
Exemple : du Shapefile au Geojson ou du MapInfo au kml.
- Option gérer les transformations de projection.

``` python
from pathlib import Path
from osgeo import ogr
from osgeo import osr

# Fonction permettant de transformer un fichier vectoriel d'un format à un autre.
# Optionnellement, une transformation de projection peut être appliquée.

def translator_gis(entree, sortie, proj_change=None):
    """
    Cette fonction permet de transformer un fichier vectoriel à l'aide d'OGR.
    Les formats pris en compte sont :
    - ESRI Shapefile
    - DBF
    - MapInfo File (TAB / MIF MID)
    - KML
    - CSV
    - GML
    - GeoJSON

    Paramètres :
    entree : str
        Chemin du fichier d'entrée à transformer.
    sortie : str
        Chemin du fichier de sortie.
    proj_change : list, optionnel
        Liste contenant les codes EPSG pour la transformation : [EPSG d'entrée, EPSG de sortie].

    Retour :
    Pas de retour.
    """

    # Détection automatique du format du fichier d'entrée et de sortie
    def format_gis(adresse):
        """
        Détermine le format OGR d'un fichier basé sur son extension.

        Paramètre :
        adresse : str
            Chemin du fichier à analyser.

        Retour :
        tuple (str, str)
            Le format du fichier et son extension en majuscules.
        """
        p = Path(adresse)
        extension = p.suffix.lower()
        if extension == '.shp':
            format_file = 'ESRI Shapefile'
        elif extension == '.dbf':
            format_file = 'ESRI Shapefile'
        elif extension in {'.tab', '.mif', '.mid'}:
            format_file = 'MapInfo File'
        elif extension == '.kml':
            format_file = 'KML'
        elif extension == '.csv':
            format_file = 'CSV'
        elif extension == '.gml':
            format_file = 'GML'
        elif extension == '.geojson':
            format_file = 'GeoJSON'
        else:
            raise Exception('Format non pris en charge par translator_gis')
        return format_file, extension.upper()

    # Détection des types de géométrie présents dans une couche
    def geometry_gis(layer, format_file):
        """
        Identifie les types de géométries présents dans une couche.

        Paramètres :
        layer : ogr.Layer
            La couche à analyser.
        format_file : str
            Format du fichier (ex : CSV, Shapefile, etc.)

        Retour :
        int
            Code de géométrie OGR (ex : wkbPoint, wkbPolygon, etc.).
        """
        geo_type = []
        for i in range(layer.GetFeatureCount()):
            feature = layer.GetFeature(i)
            if feature.GetGeometryRef():
                feature_geo_type = feature.GetGeometryRef().GetGeometryType()
                if feature_geo_type not in geo_type:
                    geo_type.append(feature_geo_type)
            else:
                break

        if len(geo_type) == 0:
            result = ogr.wkbNone
        elif len(geo_type) == 1:
            result = geo_type[0]
        elif len(geo_type) == 2:
            if {geo_type[0], geo_type[1]} == {ogr.wkbPoint, ogr.wkbMultiPoint}:
                result = ogr.wkbMultiPoint
            elif {geo_type[0], geo_type[1]} == {ogr.wkbLineString, ogr.wkbMultiLineString}:
                result = ogr.wkbMultiLineString
            elif {geo_type[0], geo_type[1]} == {ogr.wkbPolygon, ogr.wkbMultiPolygon}:
                result = ogr.wkbMultiPolygon
            else:
                result = ogr.wkbGeometryCollection
        else:
            result = ogr.wkbGeometryCollection
        return result

    # Ouverture du fichier d'entrée
    in_format_file, in_extension = format_gis(entree)
    in_driver = ogr.GetDriverByName(in_format_file)
    in_data_source = in_driver.Open(entree, 0)
    in_layer = in_data_source.GetLayer()
    in_proj_ref = in_layer.GetSpatialRef()

    # Création du fichier de sortie
    out_format_file, out_extension = format_gis(sortie)
    out_driver = ogr.GetDriverByName(out_format_file)
    out_data_source = out_driver.CreateDataSource(sortie)

    # Détermination du type de géométrie pour le fichier de sortie
    if out_extension in {'.CSV', '.DBF'}:
        out_geo_type = ogr.wkbNone
        out_proj_ref = None
    else:
        out_geo_type = geometry_gis(in_layer, in_format_file)
        out_proj_ref = in_proj_ref

    # Gestion de la transformation de projection
    if proj_change and out_geo_type != ogr.wkbNone:
        in_layer_proj = osr.SpatialReference()
        in_layer_proj.ImportFromEPSG(proj_change[0])
        out_layer_proj = osr.SpatialReference()
        out_layer_proj.ImportFromEPSG(proj_change[1])
        out_proj_ref = out_layer_proj

    # Création de la nouvelle couche dans le fichier de sortie
    out_layer = out_data_source.CreateLayer(in_layer.GetName(), out_proj_ref, out_geo_type)

    # Création des champs dans la couche de sortie
    for i in range(in_layer.GetLayerDefn().GetFieldCount()):
        in_field = in_layer.GetLayerDefn().GetFieldDefn(i)
        out_field = ogr.FieldDefn(in_field.GetName(), in_field.GetType())
        out_field.SetWidth(in_field.GetWidth())
        out_field.SetPrecision(in_field.GetPrecision())
        out_layer.CreateField(out_field)

    # Copie des entités de la couche d'entrée vers la couche de sortie
    for in_feature in in_layer:
        out_feature = in_feature.Clone()
        if proj_change and out_geo_type != ogr.wkbNone:
            in_geometry = in_feature.GetGeometryRef()
            in_geometry.AssignSpatialReference(in_layer_proj)
            in_geometry.TransformTo(out_layer_proj)
            out_feature.SetGeometry(in_geometry)
        if out_geo_type == ogr.wkbNone:
            out_feature.SetGeometry(None)
        out_layer.CreateFeature(out_feature)

    out_data_source = None  # Fermeture du fichier de sortie

# Exécution de la fonction
entree = "data/input/Point.shp"
sortie = "data/output/select_point_translate.geojson"
translator_gis(entree=entree, sortie=sortie, proj_change=[2154, 4326])
```

## V - Les opérateurs spatiaux

### Les testeurs spatiaux (booléen)

Ces fonctions opèrent sur les géométries et renvoient une valeur vraie ou fausse si la fonction est
vérifiée ou non.

| Fonction OGR                | Signification                                             |
|-----------------------------|-----------------------------------------------------------|
| Intersect() et Intersects() | La géométrie intersecte                                   |
| Equal() et Equals()         | La géométrie est identique à                              |
| Disjoint()                  | La géométrie est disjointe (n’intersecte pas)             |
| Touches()                   | La géométrie touche                                       |
| Crosses()                   | La géométrie croise (une partie dedans une partie dehors) |
| Within()                    | La géométrie est inclus dans                              |
| Contains()                  | La géométrie contient totalement                          |
| Overlaps()                  | La géométrie recouvre/chevauche                           |

Les géométries que l’on va tester dans les exemples suivants :

![Géométries de test](https://framagit.org/Guilhain/manuels/-/raw/bab50bf57e2f8dda4d7289a82453b4344e2f0129/OGR_PYTHON/img/OGR_operateurs_spatiaux.png)

``` python
from osgeo import ogr

# Ouverture du fichier de points
point_driver = ogr.GetDriverByName("ESRI Shapefile")
point_data_source = point_driver.Open("data/Point.shp", 0)
point_layer = point_data_source.GetLayer()

# Ouverture du fichier de lignes
ligne_driver = ogr.GetDriverByName("ESRI Shapefile")
ligne_data_source = ligne_driver.Open("data/Ligne.shp", 0)
ligne_layer = ligne_data_source.GetLayer()

# Ouverture du fichier de polygones
polygone_driver = ogr.GetDriverByName("ESRI Shapefile")
polygone_data_source = polygone_driver.Open("data/Polygon.shp", 0)
polygone_layer = polygone_data_source.GetLayer()

# Recherche des lignes qui intersectent le premier polygone
# Récupération de la géométrie du premier polygone
polygone_feature = polygone_layer.GetFeature(0)
polygone_geometry = polygone_feature.GetGeometryRef()

# Boucle sur toutes les entités de la couche de lignes
for i in range(ligne_layer.GetFeatureCount()):
    ligne_feature = ligne_layer.GetFeature(i)
    ligne_geometry = ligne_feature.GetGeometryRef()

    # Vérification de l'intersection entre la ligne et le polygone
    if ligne_geometry.Intersect(polygone_geometry):
        print(f"{ligne_feature.GetField('NOM')} intersecte {polygone_feature.GetField('NOM')}")

        # Vérification si la ligne est totalement contenue dans le polygone
        if polygone_geometry.Contains(ligne_geometry):
            print(f"{ligne_feature.GetField('NOM')} est totalement compris dans {polygone_feature.GetField('NOM')}")

# Vérification si la troisième ligne croise un des polygones
ligne_feature = ligne_layer.GetFeature(2)
ligne_geometry = ligne_feature.GetGeometryRef()

# Boucle sur toutes les entités de la couche de polygones
for i in range(polygone_layer.GetFeatureCount()):
    polygone_feature = polygone_layer.GetFeature(i)
    polygone_geometry = polygone_feature.GetGeometryRef()

    # Vérification si la ligne croise le polygone
    if ligne_geometry.Crosses(polygone_geometry):
        print(f"{ligne_feature.GetField('NOM')} croise le {polygone_feature.GetField('NOM')}")

# Recherche des points situés à une distance de 0,2 de la deuxième ligne
ligne_feature = ligne_layer.GetFeature(1)
ligne_geometry = ligne_feature.GetGeometryRef()

# Création d'un buffer autour de la ligne (qui devient un polygone représentant la zone autour de la ligne)
ligne_buffer = ligne_geometry.Buffer(0.2)
print(f"Type de géométrie : {ligne_buffer.GetGeometryType()}")

# Boucle sur toutes les entités de la couche de points
for i in range(point_layer.GetFeatureCount()):
    point_feature = point_layer.GetFeature(i)
    point_geometry = point_feature.GetGeometryRef()

    # Vérification si le point est situé à une distance inférieure à 0,2 de la ligne
    if point_geometry.Intersect(ligne_buffer):
        print(f"{point_feature.GetField('NOM')} est situé à 0,2 de distance de {ligne_feature.GetField('NOM')}")
```


### Les fonctions de géotraitement (agit sur la géométrie)

Les fonctions de géotraitements agissent également sur deux géométries et renvoient en retour une
géométrie correspondant à la fonction désirée.

| Fonction OGR                            | Signification                                            |
|-----------------------------------------|----------------------------------------------------------|
| Buffer()                                | Zone tampon (distance à indiquer en paramètre)           |
| Union, UnionCascaded()                  | Union d’une géométrie avec une autre                     |
| Intersection()                          | Géométrie résultat de l’intersection                     |
| Difference()                            | Différence                                               |
| SymetricDifference()                    | Symétrique                                               |
| Simplify() / SimplifyPreserveTopology() | Simplifie la géométrie : algorithme de Douglas Peucker   |
| ConvexHull()                            | Création d’une enveloppe englobante de type ConvexHull   |

Exemple :

``` python
from osgeo import ogr

# Ouverture du fichier de points
point_driver = ogr.GetDriverByName("ESRI Shapefile")
point_data_source = point_driver.Open("data/Point.shp", 0)
point_layer = point_data_source.GetLayer()

# Ouverture du fichier de lignes
ligne_driver = ogr.GetDriverByName("ESRI Shapefile")
ligne_data_source = ligne_driver.Open("data/Ligne.shp", 0)
ligne_layer = ligne_data_source.GetLayer()

# Recherche des points situés à une distance de 0,2 de la ligne 2
ligne_feature = ligne_layer.GetFeature(1)
ligne_geometry = ligne_feature.GetGeometryRef()

# Création d'un buffer autour de la ligne (transformant la ligne en polygone représentant la zone autour d'elle)
ligne_buffer = ligne_geometry.Buffer(0.2)
print(f"Type de géométrie : {ligne_buffer.GetGeometryType()}")

# Boucle sur toutes les entités de la couche de points
for i in range(point_layer.GetFeatureCount()):
    point_feature = point_layer.GetFeature(i)
    point_geometry = point_feature.GetGeometryRef()

    # Vérification si le point est situé à une distance inférieure à 0,2 de la ligne
    if point_geometry.Intersect(ligne_buffer):
        print(f"{point_feature.GetField('NOM')} est situé à 0,2 de distance de {ligne_feature.GetField('NOM')}")
```

Résultat :
``` python
>>>
    Type de géométrie 3
    Point 12 est située à 0,2 de distance de Ligne 2
    Point 13 est située à 0,2 de distance de Ligne 2
```


### Les filtres spatiaux

On a vu qu'il était possible de réaliser des filtres attributaires, de la même manière on peut réaliser
des filtres spatiaux.
Le filtre spatial se fait directement sur la variable `layer` via la fonction `SetSpatialFilter()`.

``` python
from osgeo import ogr

# Chemin du fichier Shapefile
fichier = "data/countries/countries.shp"

# Chargement du driver pour le format Shapefile
driver = ogr.GetDriverByName("ESRI Shapefile")

# Ouverture du fichier Shapefile
data_source = driver.Open(fichier)
layer = data_source.GetLayer()

# Application d'une sélection spatiale pour filtrer les pays situés dans le quart Sud-Est du monde
wkt_se = "POLYGON ((0 0, 180 0, 180 -90, 0 -90, 0 0))"

# Création de la géométrie OGR pour le polygone de la zone Sud-Est
geometry = ogr.CreateGeometryFromWkt(wkt_se)
layer.SetSpatialFilter(geometry)

# Affichage du nom des pays dans le résultat de la requête
for feature in layer:
    print(f"{feature.GetField('sovereignt')}")
```

Résultats : 

``` python
>>>
    Angola
    Antarctica
    France
    Australia
    Burundi
    Botswana
    Democratic Republic of the Congo
    Republic of Congo
    Fiji
    Gabon
    Indonesia
    Kenya
    Lesotho
    Madagascar
    Mozambique
    Malawi
    Namibia
    France
    New Zealand
    Papua New Guinea
    Rwanda
    Solomon Islands
    Somalia
    Swaziland
    East Timor
    United Republic of Tanzania
    Uganda
    Vanuatu
    South Africa
    Zambia
    Zimbabwe
```

## Exercice

L’objectif ici est de faire un exercice avec des données gratuites de la mairie de Paris sur le site [opendata-Paris](http://opendata.paris.fr/)
Dans cet exercice on va travailler avec les données « Hotspot Wifi » et les contours de bâti.
On se place dans la peau d’un opérateur téléphonique qui, dans une étude d’impact, voudrait savoir quels sont les immeubles d’habitation, du 19ème arrondissement de Paris, qui pourraient se trouver dans un rayon de 300 m autour d’un hotspot gratuit et profiter « abusivement » des offres gratuites d’accès à internet offertes par la mairie de Paris.
* 1ére étape : utiliser la librairie geocoder en python pour récupérer les coordonnées géolocalisées des adresses des hotspots de la mairie de Paris et en créer une couche SIG.
* 2ème étape : convertir ces données en Lambert 1 (c'est la projection dans laquelle nous sont fournis les bâtis).
* 3ème étape : faire des requêtes attributaires sur la couche du bâti pour ne travailler que sur le bâti situé dans le 19éme arrondissement.
* 4ème étape : réaliser une requête spatiale pour savoir quels sont les bâtis qui se trouvent dans un rayon de 300 mètres autour d’un hotspot.

### Partie I – Traitement préliminaire du fichier Hotspot Wifi

``` python
import os
import tqdm  # Pour installer la librairie, utilisez : pip install tqdm
import geocoder  # Pour installer la librairie, utilisez : pip install geocoder
from osgeo import ogr
from osgeo import osr

############################################################
# OUVERTURE DU FICHIER CSV
############################################################

# Chargement du driver pour le format CSV
driver = ogr.GetDriverByName("CSV")
data_source = driver.Open("data/input/Liste_sites_hotpsots_Paris_WiFi.csv", 0)
layer = data_source.GetLayer()
layer_defn = layer.GetLayerDefn()

############################################################
# CREATION DU SHAPEFILE
############################################################

# Chargement du driver pour le format Shapefile
new_driver = ogr.GetDriverByName("ESRI Shapefile")
output_path = "data/output/wifi_Paris.shp"

# Suppression du Shapefile existant (s'il existe)
if os.path.isfile(output_path):
    new_driver.DeleteDataSource(output_path)

# Création de la nouvelle source de données pour le Shapefile de sortie
new_data_source = new_driver.CreateDataSource(output_path)

# Définition du système de coordonnées pour le Shapefile en Lambert 93
l93_srs = osr.SpatialReference()
l93_srs.ImportFromEPSG(2154)

# Définition du système de coordonnées WGS84 avec axe de mappage traditionnel
# la méthode : SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
# garantit que les coordonnées sont interprétées en (x, y)
# c'est-à-dire (longitude, latitude) au lieu de (latitude, longitude)
wgs84_srs = osr.SpatialReference()
wgs84_srs.ImportFromEPSG(4326)
wgs84_srs.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)

# Création de la nouvelle couche pour le Shapefile de sortie avec le système Lambert 93
new_layer = new_data_source.CreateLayer("wifi_Paris", l93_srs, ogr.wkbPoint)

# Création des champs du Shapefile à partir des informations contenues dans le CSV
for i_feature in range(layer_defn.GetFieldCount()):
    field_defn = layer_defn.GetFieldDefn(i_feature)
    new_field = ogr.FieldDefn(field_defn.GetName(), field_defn.GetType())
    new_field.SetWidth(field_defn.GetWidth())
    new_field.SetPrecision(field_defn.GetPrecision())
    new_layer.CreateField(new_field)

############################################################
# LECTURE DU CSV ET ECRITURE DANS LE SHAPEFILE
############################################################

# Boucle de lecture des entités dans le CSV et écriture dans le Shapefile
for i_feature in tqdm.tqdm(range(layer.GetFeatureCount())):
    if i_feature == 0:
        # Ignorer la première ligne du CSV car elle contient le nom des champs
        continue

    # Création d'une nouvelle entité pour la couche de sortie
    new_feature = ogr.Feature(new_layer.GetLayerDefn())

    # Récupération de l'entité du CSV pour copier les données attributaires
    feature = layer.GetFeature(i_feature)
    for i_field in range(layer_defn.GetFieldCount()):
        # Copier la valeur de chaque champ dans la nouvelle entité
        new_feature.SetField(i_field, feature.GetField(i_field))

    # Récupération de l'adresse depuis le champ spécifique
    adresse = feature.GetField(4)

    # Utilisation de geocoder pour obtenir les coordonnées de l'adresse
    g = geocoder.arcgis(adresse)
    if g:
        # Création d'une nouvelle géométrie de type point avec les coordonnées récupérées
        wkt_geometry = f"POINT({g.json['lng']} {g.json['lat']})"
        new_geometry = ogr.CreateGeometryFromWkt(wkt_geometry)

        # Assigne le système de coordonnées WGS84 à la géométrie puis la reprojette en Lambert 93
        new_geometry.AssignSpatialReference(wgs84_srs)
        new_geometry.TransformTo(l93_srs)

        # Attribution de la géométrie à la nouvelle entité
        new_feature.SetGeometry(new_geometry)

        # Ajout de l'entité à la couche de sortie
        new_layer.CreateFeature(new_feature)
    else:
        print(f"\tAdresse {adresse} introuvable")

# Fermeture du fichier de sortie
new_data_source = None
```

### Partie II – Réalisation des requêtes attributaires et spatiales

``` python
import os
from osgeo import ogr
from osgeo import osr

##################################################################
# OUVERTURE DES FICHIERS D'ENTRÉE
##################################################################

# Ouverture de la couche bâti
bati_driver = ogr.GetDriverByName("ESRI Shapefile")
bati_data_source = bati_driver.Open("data/input/VOL_BATI_Lambert1.shp", 0)
bati_layer = bati_data_source.GetLayer()
bati_layer_defn = bati_layer.GetLayerDefn()

# Ouverture de la couche Hotspot WiFi
wifi_driver = ogr.GetDriverByName("ESRI Shapefile")
wifi_data_source = wifi_driver.Open("data/input/wifi_Paris.shp", 0)
wifi_layer = wifi_data_source.GetLayer()
wifi_layer_defn = wifi_layer.GetLayerDefn()

##################################################################
# CRÉATION DU FICHIER DE SORTIE
##################################################################

# Définition du système de coordonnées en Lambert Zone 1
lambert_1_spatial_ref = osr.SpatialReference()
lambert_1_spatial_ref.ImportFromEPSG(27571)

# Création du driver pour le Shapefile de sortie
new_driver = ogr.GetDriverByName("ESRI Shapefile")
output_path = "data/output/select_bati.shp"
if os.path.isfile(output_path):
    new_driver.DeleteDataSource(output_path)

# Création de la source de données et de la couche de sortie
new_data_source = new_driver.CreateDataSource(output_path)
new_layer = new_data_source.CreateLayer("select_bati", lambert_1_spatial_ref, ogr.wkbPolygon)

# Création des champs dans la couche de sortie en fonction de la définition des champs de la couche bâti
for i in range(bati_layer_defn.GetFieldCount()):
    field_defn = bati_layer_defn.GetFieldDefn(i)
    new_field = ogr.FieldDefn(field_defn.GetName(), field_defn.GetType())
    new_field.SetWidth(field_defn.GetWidth())
    new_field.SetPrecision(field_defn.GetPrecision())
    new_layer.CreateField(new_field)

##################################################################
# GÉOTRAITEMENTS
##################################################################

# Création d'une géométrie multipoint pour tous les points du 19e arrondissement
# Création d'une liste pour stocker les coordonnées des points
multi_list = []
for i in range(wifi_layer.GetFeatureCount()):
    wifi_feature = wifi_layer.GetFeature(i)
    if wifi_feature.GetField("CP") == "75019":
        wifi_geometry = wifi_feature.GetGeometryRef()
        # Reprojection de la géométrie en Lambert Zone 1
        wifi_geometry.TransformTo(lambert_1_spatial_ref)
        # Récupération des coordonnées en format JSON puis conversion en dictionnaire
        multi_list.append(eval(wifi_geometry.ExportToJson())["coordinates"])

# Stockage des coordonnées au format GeoJSON
json_dico = {"type": "MultiPoint", "coordinates": multi_list}

# Création d'une géométrie OGR multipoint
wifi_multi_geometry = ogr.CreateGeometryFromJson(str(json_dico))
# Création d'une zone tampon autour des points WiFi
wifi_multi_geometry_buffer = wifi_multi_geometry.Buffer(100)

##################################################################
# SÉLECTION DES BÂTIS DANS LE 19E ARRONDISSEMENT
##################################################################

# Application d'un filtre attributaire pour sélectionner uniquement les bâtis du 19e arrondissement
bati_layer.SetAttributeFilter("N_AR = '19'")

# Parcours des entités de la couche bâti pour effectuer la requête attributaire et spatiale
for bati_feature in bati_layer:
    bati_geometry = bati_feature.GetGeometryRef()
    # Création de l'enveloppe de la géométrie bâti en format GeoJSON
    bati_envelope = bati_geometry.GetEnvelope()
    json_envelope = {
        "type": "Polygon",
        "coordinates": [
            [
                [bati_envelope[0], bati_envelope[2]],
                [bati_envelope[0], bati_envelope[3]],
                [bati_envelope[1], bati_envelope[3]],
                [bati_envelope[1], bati_envelope[2]],
                [bati_envelope[0], bati_envelope[2]],
            ]
        ],
    }
    bati_envelope_geometry = ogr.CreateGeometryFromJson(str(json_envelope))

    # Vérification si l'enveloppe de la géométrie bâti intersecte le buffer WiFi
    if bati_envelope_geometry.Intersect(wifi_multi_geometry_buffer):
        # Vérification si le bâti intersecte réellement la zone tampon
        if bati_geometry.Intersect(wifi_multi_geometry_buffer):
            # Clonage de l'entité bâti pour la couche de sortie
            new_feature = bati_feature.Clone()
            # Calcul de l'intersection de la géométrie bâti avec le buffer WiFi
            bati_geometry = bati_geometry.Intersection(wifi_multi_geometry_buffer)
            new_feature.SetGeometry(bati_geometry)
            new_layer.CreateFeature(new_feature)
            new_feature.Destroy()

# Fermeture du fichier de sortie
new_data_source.Destroy()
print("Traitement terminé")
```

## Conclusion

Comme vous avez pu le voir, cette librairie vous permet, avec un peu de pratique, d’accéder très rapidement à vos données SIG. Elle vous donnera l'occasion de réaliser des outils de traitement de la donnée et de la géométrie à la fois simples et sophistiqués tout en restant dans les standards préconisés par l’OGC. Il est surtout remarquable de voir qu’il est possible, vous l’avez constaté dans les exercices proposés, de réaliser ce que font certains logiciels SIG propriétaires nécessitant d’acquérir de coûteuses licences.
J’opposerai toutefois un bémol en soulignant l’absence de possibilité d’indexation des données et des géométries. Cela permettrait de réaliser des géotraitements dans des délais raisonnables, dès lors que l’on travaille sur un nombre conséquent de données, mais ceci devrait être pris en compte dans la version 2.0 de cette librairie.

## Bibliographie non exhaustive

E. Westra, *Python Geospatial Development*, 2010.

Tutoriel de Yves Jacolin
[http://gdal.gloobe.org/](http://gdal.gloobe.org/)

Tutoriel de Chris Garrard (traite également de la librairie GDAL raster)
[http://www.gis.usu.edu/~chrisg/python/](http://www.gis.usu.edu/~chrisg/python/)

Des exemples basiques pour l’utilisation d’OGR :
[http://pcjericks.github.io/py-gdalogr-cookbook/](http://pcjericks.github.io/py-gdalogr-cookbook/)

Evidemment vous pouvez me contacter à l’adresse suivante :
<g.averlant@mailfence.com>



