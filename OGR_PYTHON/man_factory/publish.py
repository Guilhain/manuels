import re
import os

def add_table_of_contents(file_path, output_file_path):
    with open(file_path, 'r') as file:
        copy_file = str(file.read())
        table_of_contents_list = ['## Table des matières']
        for line in copy_file.split('\n'):
            if line and line[0] == '#':
                split = line.split('#')
                title = split[-1][1:]
                if len(split)-3 >= 0:
                    indent = (len(split)-3) * '    '
                    replace_list = [(' ', '-'), ("’", ''), ("'", ''), ('/', ''), (':', ''), ('(', ''), (')', ''), ('-?-','-'), ('-?', '-'), ('---', '-'), ('-–-', '-'), ('--', '-')]
                    link = title
                    for rep_tuple in replace_list:
                        link = link.replace(*rep_tuple)
                    link = link.lower()
                    table_line_formated = f'{indent}* [{title}](#{link})'
                    table_of_contents_list.append(table_line_formated)

        table_of_contents_formated = '\n'.join(table_of_contents_list)

        copy_file= copy_file.replace('~< table_of_contents <~', table_of_contents_formated)

        with open(output_file_path, "w") as output_file:
            output_file.write(copy_file)

def add_code(file_path, output_file_path):
    with open(file_path, 'r') as file:
        copy_file = str(file.read())
        stop = False
        it = 0
        while stop is False:
            code_insert_search = re.search('<~.*?~>', copy_file)
            if not code_insert_search:
                stop = True
            else:
                string_find = code_insert_search.group(0)
                path = '../'+string_find.replace("<~ ", "").replace(' ~>', '')

                with open(path, 'r') as code_file:
                    code_str = code_file.read()
                code_str = f"""``` python\n{code_str}```"""
                copy_file = copy_file.replace(string_find, code_str)
                it += 1

        with open(output_file_path, "w") as output_file:
            output_file.write(copy_file)

def copy_in_root(file_path, output_path):
    os.system(f"cp {file_path} {output_path}")

def test_before_publish(file_path):

    with open(file_path, 'r') as file:
        pattern = '<~'
        if '<~' in file.read():
            extract = file.split(pattern)[1][:100]
            raise Exception(f'Pattern {pattern} found in md. extract : {extract}')

def publish_man():
    add_table_of_contents(file_path='00_CoursPython-GeospatialPython_without_table_of_contents_and_code.md',
                          output_file_path="01_CoursPython-GeospatialPython_without_code.md")
    add_code(file_path='01_CoursPython-GeospatialPython_without_code.md',
             output_file_path="02_CoursPython-GeospatialPython.md")
    test_before_publish(file_path="02_CoursPython-GeospatialPython.md")
    copy_in_root(file_path='02_CoursPython-GeospatialPython.md',
                 output_path='../CoursPython-GeospatialPython.md')


if __name__ == '__main__':
    publish_man()