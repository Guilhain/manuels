# Manipulations spatiales et attributaires avec OGR en Python

![](https://framagit.org/Guilhain/manuels/-/raw/1357f186846cc59b1643c9652f1c70f34c9d1005/OGR_PYTHON/img/logo_250x250.png)


Rédigé par Guilhain Averlant en mars 2013, dernière révision novembre 2024, 5ème édition.
Licence : Licence Creative Commons de type [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0) v. 2024-11.


> L'esprit devenu capable de fonctionner
> comme une machine se reconnaît dans
> la machine capable de fonctionner com-
> me lui — sans s'apercevoir qu'en vérité 
> la machine ne fonctionne pas comme 
> l'esprit, mais seulement comme l'esprit
> ayant appris à fonctionner comme une 
> machine.
>
> -- <cite>André Gorz</cite>

## Note de l’auteur

### Pour qui ? Pourquoi ?

Le présent manuel est le support de cours que je dispense depuis novembre 2012 sur la librairie OGR / GDAL au Master 2 Carthagéo — Université Paris 1 / Paris 7 et ENSG. Idéalement afin de pouvoir en tirer les enseignements dans leur intégralité il faudrait idéalement y consacrer au moins 24 heures d’enseignement. 
Cet ouvrage doit être vu comme un document de support dont l’idée principale est d’apprendre les bases d’OGR afin d’aller plus loin par la suite.
Même si les étudiants sont la première cible de ce manuel, il est plus largement destiné à l'ensemble de ceux qui utilisent Python.

### L’idée fondatrice

A mes yeux cet ouvrage et les enseignements qu’il dispense s’inscrivent dans une démarche de conquête — voire reconquête — de la conception des outils informatiques en cartographie. En effet nous notons tous la prédominance de certains acteurs, propriétaires comme libre, dans les logiciels de géomatique. Cette prédominance est une preuve flagrante de l’intérêt que notre matière suscite auprès du secteur privé/public mais également pour le grand public.
J’observe pourtant un fort attachement, si ce n’est dépendance aux outils, au détriment de la connaissance, du savoir, qui a permis de les constituer. 
Ainsi l’idée fondatrice est de permettre à l’étudiant, mais aussi au cartographe, de repenser ce qu’il se passe derrière l’outil, comment celui-ci «masse» la donnée pour en sortir un résultat graphique ou statistique conforme aux attentes de l’utilisateur.

### Quelle licence ?

Après la rédaction, pour le moins fastidieuse, de ce manuel, s’est posée la question de sa licence de diffusion. Il fallait que sa distribution soit gratuite, et le reste, mais aussi que les utilisateurs puissent le modifier et le partager à leur guise.
C’est donc tout naturellement que je me suis dirigé vers le dispositif de licence Creative Commons de type CC BY-SA.

~< table_of_contents <~

## Introduction

Vous avez désormais vu les rudiments du langage de programmation python. Désormais, nous allons voir comment ce langage peut être utilisé pour manipuler de l’information géospatiale afin de réaliser des outils d’exploitation des données et des géotraitements avec vos fichiers SIG.
	
Dans ce tutoriel introductif nous ne traiterons que des données vectorielles 2D mais il faut savoir que la librairie OGR/GDAL peut exploiter les données 2,5D.


## I - Installation

Autant le dire tout de suite, OGR/GDAL n'est pas une librairie très simple à installer, du moins quand on est pas utilisateur de linux.
Pour fonctionner, elle est dépendante d'un grand nombre de librairies. Toutefois, pas de panique, des utilisateurs bienveillants ont réalisé des packages prêts à l'emploi parfaitement satisfaisants pour une utilisation dans les formats les plus utilisés.

### Windows

Nous vous conseillons de suivre les étapes décritent par cet [article](https://opensourceoptions.com/blog/how-to-install-gdal-for-python-with-pip-on-windows/)

### Mac OS

Pour les utilisateurs de Mac OS nous conseillons de suivre les indications de cet [article](https://medium.com/@egiron/how-to-install-gdal-and-qgis-on-macos-catalina-ca690dca4f91)

### Linux

Pour les systèmes linux le plus simple est d'utiliser la commande suivante :

```shell
sudo apt-get install python-gdal
```

Autres infos disponibles sur le site d’Yves Jacolin dédiés à OGR / GDAL  : [http://gdal.gloobe.org/install.html](http://gdal.gloobe.org/install.html)

## II - L'information géospatiale : bref rappel

Généralement on a tendance à croire que l’information géographique forme un tout, à titre personnel je préfère opérer les distinctions suivantes :
* La donnée : contenue dans la table attributaire du fichier SIG. En fonction du typage des données qui s’y trouve on peut effectuer des opérations statistiques, des classifications des sélections en fonction d’attribut …
* La géométrie : elle se résume par un type (point, ligne, polygone…) représenté par une ou des coordonnées liées les unes aux autres. On peut évidemment les simplifier, les généraliser, les fusionner …
* Un format de stockage : cet aspect est quasi toujours oublié pourtant il a toute son importance, le format conditionne bien souvent l’espace disque de la donnée, mais aussi les typages des données et des géométries qu’il supporte et enfin son accessibilité au public.

### De la donnée

La donnée se caractérise par type une longueur et parfois d’une précision.

| Code OGR | Type de la donnée |
|----------|-------------------|
| 0        | Integer           |
| 1        | IntegerList       |
| 2        | Real              |
| 3        | RealList          |
| 4        | String            |
| 5        | StringList        |
| 6        | WideString        |
| 7        | WideStringList    |
| 8        | Binary            |
| 9        | Date              |
| 10       | DateTime          |

### De la géométrie

| Code OGR    | Type de la géométrie     |
|-------------|--------------------------|
| -2147483648 | wkb25Bit                 |
| -2147483641 | wkbGeometryCollection25D |
| -2147483642 | wkbMultiPolygon25D       |
| -2147483643 | wkbMultiLineString25D    |
| -2147483644 | wkbMultiPoint25D         |
| -2147483645 | wkbPolygon25D            |
| -2147483646 | wkbLineString25D         |
| -2147483647 | wkbPoint25D              |
| -2147483648 | wkb25DBit                |
| 0           | wkbUnknown               |
| 0           | wkbXDR                   |
| 1           | wkbNDR                   |
| 1           | wkbPoint                 |
| 100         | wkbNone                  |
| 101         | wkbLinearRing            |
| 2           | wkbLineString            |
| 3           | wkbPolygon               |
| 4           | wkbMultiPoint            |
| 5           | wkbMultiLineString       |
| 6           | wkbMultiPolygon          |
| 7           | wkbGeometryCollection    |


### Un format 

Même si la donnée et la géométrie sont ce qui caractérise le plus l’information d’un fichier SIG il ne faut pas négliger l’importance due au format dans lequel ces informations sont stockées. 
Le format conditionne à la fois le typage des données mais aussi les géométries. 
Par exemple concernant les données le format Shapefile tolère qu’on n’indique pas la précision des données pour des données de type double, contrairement au format Postgis qui forcera les données à apparaître en Integer si aucune précision n’est donnée. Pour la géométrie le format Shapefile tolère les self-intersection sur un polygone contrairement au format Postgis, de même le format Oracle Spatial refuse des polygones de moins de 4 coordonnées (des triangles).

## III - La librairie GDAL/OGR

La librairie GDAL/OGR est un projet open source, il faut bien être conscient que de plus en plus les projets open source en géomatique prennent de l’ampleur. Ceci au moins pour deux raisons : la première parce qu’ils arrivent à fédérer une grande communauté d’utilisateurs donc  une somme de compétences  et d’expertises vraiment considérable, ces projets sont donc techniquement très sérieux voire même plus fiables que des solutions propriétaires sur certains aspects, en particulier le support utilisateur … La seconde raison je l’attribue à des considérations financières : en effet pourquoi payer des solutions propriétaires chères si des solutions open source fonctionnent au moins aussi bien ?

Je tiens à focaliser votre attention sur cette circulaire de Jean Marc Ayrault datant du 19 septembre 2012 : 
[http://circulaire.legifrance.gouv.fr/pdf/2012/09/cir_35837.pdf]http://circulaire.legifrance.gouv.fr/pdf/2012/09/cir_35837.pdf

### GDAL/OGR qu’est-ce ?

Date de création : 1998
Créateur : Franck Wanderman
Licence : X11 / MIT
Un ensemble de librairies d’abord développées en C++ mais désormais disponibles pour 7 autres languages dont le Python. 

Ces librairies sont : 
* GDAL : traitement des images raster, spécifiquement utilisée pour la manipulation des images et la télédétection.
* OGR : traitement de fichiers vectoriels
* OSR : traitement des projections

### les formats vectoriels

Actuellement OGR/GDAL gère 81 formats vectoriels différents les plus utilises sont :

| Code OGR               | Format de fichier         |
|------------------------|---------------------------|
| ESRI Shapefile	        | ESRI Shapefile            |
| Geoconcept	            | Geoconcept                |
| Geojson                | Geojson                   |
| GPKG                   | GeoPackage    |
| GML                    | Geography Markup Language |
| KML	                   | Keyhole Markup Language   |
| MapInfo File	          | Map Info                  |
| Mysql                  | Mysql                     |
| OCI	                   |Oracle Spatial             |
| OpenFileGDB            |Geodatabase fichier        |
| PostgreSQL ou PostGIS	 | Postgres ou Postgis       |
| SDE	                   | ESRI SDE                  |

La liste des formats disponibles pour OGR est ici : 
[http://www.gdal.org/ogr/ogr_formats.html](http://www.gdal.org/ogr/ogr_formats.html)


## IV - Manipulation de base

Avant de commencer, il est nécessaire de savoir que l'entièreté des fonctions d'OGR est consultable ici : [http://gdal.org/python/](http://gdal.org/python/)

Les principales fonctions pour lire un fichier SIG peuvent être résumées par ce schéma ci-dessous :
![Schema : lecture avec OGR](https://framagit.org/Guilhain/manuels/-/raw/7029990fde6a2bc1b8adb104c8b4f2249f449696/OGR_PYTHON/img/OGR_Lecture.png)

### Appeler la libraire OGR

``` python
from osgeo import ogr
```

### Ouvrir un fichier Shapefile

<~ exemples/00_lecture.py ~>


### Ecrire un fichier au format Postgis

Voici un schéma résumant comment créer un fichier SIG :

![Schema  : écriture avec OGR](https://framagit.org/Guilhain/manuels/-/raw/7029990fde6a2bc1b8adb104c8b4f2249f449696/OGR_PYTHON/img/OGR_Ecriture.png)

<~ exemples/01_ecriture.py ~>


### Supprimer un fichier SIG

Certains formats, par exemple le format ESRI Shapefile pour ne pas le citer, peut comporter une multitude de fichiers, dans notre exemple à minima trois voir plus. Il peut donc paraitre fastidieux de les supprimer d'un répertoire. Heureusement OGR permet grâce à la fonction ```DeleteDataSource()```.

<~ exemples/02_suppression.py ~>

### Récupérer une projection

Une fois un fichier vecteur ouvert avec OGR une simple requête sur l’objet `layer.GetSpatialRef()` permet de récupérer le système de projection utilisée.

<~ exemples/03_projection_get_spatial_ref.py ~>

Résultat

``` python
>>> GEOGCS["GCS_WGS_1984",
    DATUM["WGS_1984",
        SPHEROID["WGS_1984",6378137,298.257223563]],
    PRIMEM["Greenwich",0],
    UNIT["Degree",0.017453292519943295]]
```

### Atributer une projection

Lorsque l’on crée un fichier vectoriel, on a la possibilité d’indiquer une projection dans les paramètres de la fonction `CreateLayer()`.
Pour renseigner la projection que l’on veut choisir on va utiliser la librairie OSR.
Le site Spatial Reference permet de retrouver les codes de projections et ce sous différents formats, pour moi ce site fait référence : [http://spatialreference.org/](http://spatialreference.org/)

<~ exemples/04_get_projection.py ~>

Résultat :

``` python
>>> 
GEOGCS["GCS_WGS_1984",
    DATUM["WGS_1984",
        SPHEROID["WGS_1984",6378137,298.257223563]],
    PRIMEM["Greenwich",0],
    UNIT["Degree",0.017453292519943295]]
GEOGCS["WGS 84",
    DATUM["WGS_1984",
        SPHEROID["WGS 84",6378137,298.257223563,
            AUTHORITY["EPSG","7030"]],
        AUTHORITY["EPSG","6326"]],
    PRIMEM["Greenwich",0,
        AUTHORITY["EPSG","8901"]],
    UNIT["degree",0.0174532925199433,
        AUTHORITY["EPSG","9122"]],
    AUTHORITY["EPSG","4326"]]
```

### Réaliser une transformation de projection

<~ exemples/05_projection_reproject_geometry.py ~>

Résultat:

``` python
>>> Nouvelles coordonnées de Paris en RGF93 : POINT (6965934.894937987439334 3096339.348330470267683)
```

### Les filtres attributaires

Parfois on a pas envie de travailler sur un jeu de données en entier et on a pas envie non plus de faire une boucle sur les attributs d'une couche. Pour ce faire on peut faire un filtre attributaire.
Le filtre attributaire se fait directement sur la variable layer via la fonction `SetAttributeFilter()`.

<~ exemples/06_filtre_attributaire.py ~>

### Exercice

Créer un outil de conversion de format de fichier vectoriel.
Exemple : du Shapefile au Geojson ou du MapInfo au kml.
- Option gérer les transformations de projection.

<~ exercices/01_translator_gis.py ~>

## V - Les opérateurs spatiaux

### Les testeurs spatiaux (booléen)

Ces fonctions opèrent sur les géométries et renvoient une valeur vraie ou fausse si la fonction est
vérifiée ou non.

| Fonction OGR                | Signification                                             |
|-----------------------------|-----------------------------------------------------------|
| Intersect() et Intersects() | La géométrie intersecte                                   |
| Equal() et Equals()         | La géométrie est identique à                              |
| Disjoint()                  | La géométrie est disjointe (n’intersecte pas)             |
| Touches()                   | La géométrie touche                                       |
| Crosses()                   | La géométrie croise (une partie dedans une partie dehors) |
| Within()                    | La géométrie est inclus dans                              |
| Contains()                  | La géométrie contient totalement                          |
| Overlaps()                  | La géométrie recouvre/chevauche                           |

Les géométries que l’on va tester dans les exemples suivants :

![Géométries de test](https://framagit.org/Guilhain/manuels/-/raw/bab50bf57e2f8dda4d7289a82453b4344e2f0129/OGR_PYTHON/img/OGR_operateurs_spatiaux.png)

<~ exemples/07_geoprocess_return_boolean.py ~>


### Les fonctions de géotraitement (agit sur la géométrie)

Les fonctions de géotraitements agissent également sur deux géométries et renvoient en retour une
géométrie correspondant à la fonction désirée.

| Fonction OGR                            | Signification                                            |
|-----------------------------------------|----------------------------------------------------------|
| Buffer()                                | Zone tampon (distance à indiquer en paramètre)           |
| Union, UnionCascaded()                  | Union d’une géométrie avec une autre                     |
| Intersection()                          | Géométrie résultat de l’intersection                     |
| Difference()                            | Différence                                               |
| SymetricDifference()                    | Symétrique                                               |
| Simplify() / SimplifyPreserveTopology() | Simplifie la géométrie : algorithme de Douglas Peucker   |
| ConvexHull()                            | Création d’une enveloppe englobante de type ConvexHull   |

Exemple :

<~ exemples/08_geoprocess_return_geometry.py ~>

Résultat :
``` python
>>>
    Type de géométrie 3
    Point 12 est située à 0,2 de distance de Ligne 2
    Point 13 est située à 0,2 de distance de Ligne 2
```


### Les filtres spatiaux

On a vu qu'il était possible de réaliser des filtres attributaires, de la même manière on peut réaliser
des filtres spatiaux.
Le filtre spatial se fait directement sur la variable `layer` via la fonction `SetSpatialFilter()`.

<~ exemples/09_filtre_spatial.py ~>

Résultats : 

``` python
>>>
    Angola
    Antarctica
    France
    Australia
    Burundi
    Botswana
    Democratic Republic of the Congo
    Republic of Congo
    Fiji
    Gabon
    Indonesia
    Kenya
    Lesotho
    Madagascar
    Mozambique
    Malawi
    Namibia
    France
    New Zealand
    Papua New Guinea
    Rwanda
    Solomon Islands
    Somalia
    Swaziland
    East Timor
    United Republic of Tanzania
    Uganda
    Vanuatu
    South Africa
    Zambia
    Zimbabwe
```

## Exercice

L’objectif ici est de faire un exercice avec des données gratuites de la mairie de Paris sur le site [opendata-Paris](http://opendata.paris.fr/)
Dans cet exercice on va travailler avec les données « Hotspot Wifi » et les contours de bâti.
On se place dans la peau d’un opérateur téléphonique qui, dans une étude d’impact, voudrait savoir quels sont les immeubles d’habitation, du 19ème arrondissement de Paris, qui pourraient se trouver dans un rayon de 300 m autour d’un hotspot gratuit et profiter « abusivement » des offres gratuites d’accès à internet offertes par la mairie de Paris.
* 1ére étape : utiliser la librairie geocoder en python pour récupérer les coordonnées géolocalisées des adresses des hotspots de la mairie de Paris et en créer une couche SIG.
* 2ème étape : convertir ces données en Lambert 1 (c'est la projection dans laquelle nous sont fournis les bâtis).
* 3ème étape : faire des requêtes attributaires sur la couche du bâti pour ne travailler que sur le bâti situé dans le 19éme arrondissement.
* 4ème étape : réaliser une requête spatiale pour savoir quels sont les bâtis qui se trouvent dans un rayon de 300 mètres autour d’un hotspot.

### Partie I – Traitement préliminaire du fichier Hotspot Wifi

<~ exercices/02_partie_1_creation_hotspot_wifi.py ~>

### Partie II – Réalisation des requêtes attributaires et spatiales

<~ exercices/02_Partie_2_requetes_spatiales.py ~>

## Conclusion

Comme vous avez pu le voir, cette librairie vous permet, avec un peu de pratique, d’accéder très rapidement à vos données SIG. Elle vous donnera l'occasion de réaliser des outils de traitement de la donnée et de la géométrie à la fois simples et sophistiqués tout en restant dans les standards préconisés par l’OGC. Il est surtout remarquable de voir qu’il est possible, vous l’avez constaté dans les exercices proposés, de réaliser ce que font certains logiciels SIG propriétaires nécessitant d’acquérir de coûteuses licences.
J’opposerai toutefois un bémol en soulignant l’absence de possibilité d’indexation des données et des géométries. Cela permettrait de réaliser des géotraitements dans des délais raisonnables, dès lors que l’on travaille sur un nombre conséquent de données, mais ceci devrait être pris en compte dans la version 2.0 de cette librairie.

## Bibliographie non exhaustive

E. Westra, *Python Geospatial Development*, 2010.

Tutoriel de Yves Jacolin
[http://gdal.gloobe.org/](http://gdal.gloobe.org/)

Tutoriel de Chris Garrard (traite également de la librairie GDAL raster)
[http://www.gis.usu.edu/~chrisg/python/](http://www.gis.usu.edu/~chrisg/python/)

Des exemples basiques pour l’utilisation d’OGR :
[http://pcjericks.github.io/py-gdalogr-cookbook/](http://pcjericks.github.io/py-gdalogr-cookbook/)

Evidemment vous pouvez me contacter à l’adresse suivante :
<g.averlant@mailfence.com>



