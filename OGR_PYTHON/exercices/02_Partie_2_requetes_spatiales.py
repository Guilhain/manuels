import os
from osgeo import ogr
from osgeo import osr

##################################################################
# OUVERTURE DES FICHIERS D'ENTRÉE
##################################################################

# Ouverture de la couche bâti
bati_driver = ogr.GetDriverByName("ESRI Shapefile")
bati_data_source = bati_driver.Open("data/input/VOL_BATI_Lambert1.shp", 0)
bati_layer = bati_data_source.GetLayer()
bati_layer_defn = bati_layer.GetLayerDefn()

# Ouverture de la couche Hotspot WiFi
wifi_driver = ogr.GetDriverByName("ESRI Shapefile")
wifi_data_source = wifi_driver.Open("data/input/wifi_Paris.shp", 0)
wifi_layer = wifi_data_source.GetLayer()
wifi_layer_defn = wifi_layer.GetLayerDefn()

##################################################################
# CRÉATION DU FICHIER DE SORTIE
##################################################################

# Définition du système de coordonnées en Lambert Zone 1
lambert_1_spatial_ref = osr.SpatialReference()
lambert_1_spatial_ref.ImportFromEPSG(27571)

# Création du driver pour le Shapefile de sortie
new_driver = ogr.GetDriverByName("ESRI Shapefile")
output_path = "data/output/select_bati.shp"
if os.path.isfile(output_path):
    new_driver.DeleteDataSource(output_path)

# Création de la source de données et de la couche de sortie
new_data_source = new_driver.CreateDataSource(output_path)
new_layer = new_data_source.CreateLayer("select_bati", lambert_1_spatial_ref, ogr.wkbPolygon)

# Création des champs dans la couche de sortie en fonction de la définition des champs de la couche bâti
for i in range(bati_layer_defn.GetFieldCount()):
    field_defn = bati_layer_defn.GetFieldDefn(i)
    new_field = ogr.FieldDefn(field_defn.GetName(), field_defn.GetType())
    new_field.SetWidth(field_defn.GetWidth())
    new_field.SetPrecision(field_defn.GetPrecision())
    new_layer.CreateField(new_field)

##################################################################
# GÉOTRAITEMENTS
##################################################################

# Création d'une géométrie multipoint pour tous les points du 19e arrondissement
# Création d'une liste pour stocker les coordonnées des points
multi_list = []
for i in range(wifi_layer.GetFeatureCount()):
    wifi_feature = wifi_layer.GetFeature(i)
    if wifi_feature.GetField("CP") == "75019":
        wifi_geometry = wifi_feature.GetGeometryRef()
        # Reprojection de la géométrie en Lambert Zone 1
        wifi_geometry.TransformTo(lambert_1_spatial_ref)
        # Récupération des coordonnées en format JSON puis conversion en dictionnaire
        multi_list.append(eval(wifi_geometry.ExportToJson())["coordinates"])

# Stockage des coordonnées au format GeoJSON
json_dico = {"type": "MultiPoint", "coordinates": multi_list}

# Création d'une géométrie OGR multipoint
wifi_multi_geometry = ogr.CreateGeometryFromJson(str(json_dico))
# Création d'une zone tampon autour des points WiFi
wifi_multi_geometry_buffer = wifi_multi_geometry.Buffer(100)

##################################################################
# SÉLECTION DES BÂTIS DANS LE 19E ARRONDISSEMENT
##################################################################

# Application d'un filtre attributaire pour sélectionner uniquement les bâtis du 19e arrondissement
bati_layer.SetAttributeFilter("N_AR = '19'")

# Parcours des entités de la couche bâti pour effectuer la requête attributaire et spatiale
for bati_feature in bati_layer:
    bati_geometry = bati_feature.GetGeometryRef()
    # Création de l'enveloppe de la géométrie bâti en format GeoJSON
    bati_envelope = bati_geometry.GetEnvelope()
    json_envelope = {
        "type": "Polygon",
        "coordinates": [
            [
                [bati_envelope[0], bati_envelope[2]],
                [bati_envelope[0], bati_envelope[3]],
                [bati_envelope[1], bati_envelope[3]],
                [bati_envelope[1], bati_envelope[2]],
                [bati_envelope[0], bati_envelope[2]],
            ]
        ],
    }
    bati_envelope_geometry = ogr.CreateGeometryFromJson(str(json_envelope))

    # Vérification si l'enveloppe de la géométrie bâti intersecte le buffer WiFi
    if bati_envelope_geometry.Intersect(wifi_multi_geometry_buffer):
        # Vérification si le bâti intersecte réellement la zone tampon
        if bati_geometry.Intersect(wifi_multi_geometry_buffer):
            # Clonage de l'entité bâti pour la couche de sortie
            new_feature = bati_feature.Clone()
            # Calcul de l'intersection de la géométrie bâti avec le buffer WiFi
            bati_geometry = bati_geometry.Intersection(wifi_multi_geometry_buffer)
            new_feature.SetGeometry(bati_geometry)
            new_layer.CreateFeature(new_feature)
            new_feature.Destroy()

# Fermeture du fichier de sortie
new_data_source.Destroy()
print("Traitement terminé")
