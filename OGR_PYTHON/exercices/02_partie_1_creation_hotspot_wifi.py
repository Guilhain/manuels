import os
import tqdm  # Pour installer la librairie, utilisez : pip install tqdm
import geocoder  # Pour installer la librairie, utilisez : pip install geocoder
from osgeo import ogr
from osgeo import osr

############################################################
# OUVERTURE DU FICHIER CSV
############################################################

# Chargement du driver pour le format CSV
driver = ogr.GetDriverByName("CSV")
data_source = driver.Open("data/input/Liste_sites_hotpsots_Paris_WiFi.csv", 0)
layer = data_source.GetLayer()
layer_defn = layer.GetLayerDefn()

############################################################
# CREATION DU SHAPEFILE
############################################################

# Chargement du driver pour le format Shapefile
new_driver = ogr.GetDriverByName("ESRI Shapefile")
output_path = "data/output/wifi_Paris.shp"

# Suppression du Shapefile existant (s'il existe)
if os.path.isfile(output_path):
    new_driver.DeleteDataSource(output_path)

# Création de la nouvelle source de données pour le Shapefile de sortie
new_data_source = new_driver.CreateDataSource(output_path)

# Définition du système de coordonnées pour le Shapefile en Lambert 93
l93_srs = osr.SpatialReference()
l93_srs.ImportFromEPSG(2154)

# Définition du système de coordonnées WGS84 avec axe de mappage traditionnel
# la méthode : SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
# garantit que les coordonnées sont interprétées en (x, y)
# c'est-à-dire (longitude, latitude) au lieu de (latitude, longitude)
wgs84_srs = osr.SpatialReference()
wgs84_srs.ImportFromEPSG(4326)
wgs84_srs.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)

# Création de la nouvelle couche pour le Shapefile de sortie avec le système Lambert 93
new_layer = new_data_source.CreateLayer("wifi_Paris", l93_srs, ogr.wkbPoint)

# Création des champs du Shapefile à partir des informations contenues dans le CSV
for i_feature in range(layer_defn.GetFieldCount()):
    field_defn = layer_defn.GetFieldDefn(i_feature)
    new_field = ogr.FieldDefn(field_defn.GetName(), field_defn.GetType())
    new_field.SetWidth(field_defn.GetWidth())
    new_field.SetPrecision(field_defn.GetPrecision())
    new_layer.CreateField(new_field)

############################################################
# LECTURE DU CSV ET ECRITURE DANS LE SHAPEFILE
############################################################

# Boucle de lecture des entités dans le CSV et écriture dans le Shapefile
for i_feature in tqdm.tqdm(range(layer.GetFeatureCount())):
    if i_feature == 0:
        # Ignorer la première ligne du CSV car elle contient le nom des champs
        continue

    # Création d'une nouvelle entité pour la couche de sortie
    new_feature = ogr.Feature(new_layer.GetLayerDefn())

    # Récupération de l'entité du CSV pour copier les données attributaires
    feature = layer.GetFeature(i_feature)
    for i_field in range(layer_defn.GetFieldCount()):
        # Copier la valeur de chaque champ dans la nouvelle entité
        new_feature.SetField(i_field, feature.GetField(i_field))

    # Récupération de l'adresse depuis le champ spécifique
    adresse = feature.GetField(4)

    # Utilisation de geocoder pour obtenir les coordonnées de l'adresse
    g = geocoder.arcgis(adresse)
    if g:
        # Création d'une nouvelle géométrie de type point avec les coordonnées récupérées
        wkt_geometry = f"POINT({g.json['lng']} {g.json['lat']})"
        new_geometry = ogr.CreateGeometryFromWkt(wkt_geometry)

        # Assigne le système de coordonnées WGS84 à la géométrie puis la reprojette en Lambert 93
        new_geometry.AssignSpatialReference(wgs84_srs)
        new_geometry.TransformTo(l93_srs)

        # Attribution de la géométrie à la nouvelle entité
        new_feature.SetGeometry(new_geometry)

        # Ajout de l'entité à la couche de sortie
        new_layer.CreateFeature(new_feature)
    else:
        print(f"\tAdresse {adresse} introuvable")

# Fermeture du fichier de sortie
new_data_source = None
