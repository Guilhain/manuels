from pathlib import Path
from osgeo import ogr
from osgeo import osr

# Fonction permettant de transformer un fichier vectoriel d'un format à un autre.
# Optionnellement, une transformation de projection peut être appliquée.

def translator_gis(entree, sortie, proj_change=None):
    """
    Cette fonction permet de transformer un fichier vectoriel à l'aide d'OGR.
    Les formats pris en compte sont :
    - ESRI Shapefile
    - DBF
    - MapInfo File (TAB / MIF MID)
    - KML
    - CSV
    - GML
    - GeoJSON

    Paramètres :
    entree : str
        Chemin du fichier d'entrée à transformer.
    sortie : str
        Chemin du fichier de sortie.
    proj_change : list, optionnel
        Liste contenant les codes EPSG pour la transformation : [EPSG d'entrée, EPSG de sortie].

    Retour :
    Pas de retour.
    """

    # Détection automatique du format du fichier d'entrée et de sortie
    def format_gis(adresse):
        """
        Détermine le format OGR d'un fichier basé sur son extension.

        Paramètre :
        adresse : str
            Chemin du fichier à analyser.

        Retour :
        tuple (str, str)
            Le format du fichier et son extension en majuscules.
        """
        p = Path(adresse)
        extension = p.suffix.lower()
        if extension == '.shp':
            format_file = 'ESRI Shapefile'
        elif extension == '.dbf':
            format_file = 'ESRI Shapefile'
        elif extension in {'.tab', '.mif', '.mid'}:
            format_file = 'MapInfo File'
        elif extension == '.kml':
            format_file = 'KML'
        elif extension == '.csv':
            format_file = 'CSV'
        elif extension == '.gml':
            format_file = 'GML'
        elif extension == '.geojson':
            format_file = 'GeoJSON'
        else:
            raise Exception('Format non pris en charge par translator_gis')
        return format_file, extension.upper()

    # Détection des types de géométrie présents dans une couche
    def geometry_gis(layer, format_file):
        """
        Identifie les types de géométries présents dans une couche.

        Paramètres :
        layer : ogr.Layer
            La couche à analyser.
        format_file : str
            Format du fichier (ex : CSV, Shapefile, etc.)

        Retour :
        int
            Code de géométrie OGR (ex : wkbPoint, wkbPolygon, etc.).
        """
        geo_type = []
        for i in range(layer.GetFeatureCount()):
            feature = layer.GetFeature(i)
            if feature.GetGeometryRef():
                feature_geo_type = feature.GetGeometryRef().GetGeometryType()
                if feature_geo_type not in geo_type:
                    geo_type.append(feature_geo_type)
            else:
                break

        if len(geo_type) == 0:
            result = ogr.wkbNone
        elif len(geo_type) == 1:
            result = geo_type[0]
        elif len(geo_type) == 2:
            if {geo_type[0], geo_type[1]} == {ogr.wkbPoint, ogr.wkbMultiPoint}:
                result = ogr.wkbMultiPoint
            elif {geo_type[0], geo_type[1]} == {ogr.wkbLineString, ogr.wkbMultiLineString}:
                result = ogr.wkbMultiLineString
            elif {geo_type[0], geo_type[1]} == {ogr.wkbPolygon, ogr.wkbMultiPolygon}:
                result = ogr.wkbMultiPolygon
            else:
                result = ogr.wkbGeometryCollection
        else:
            result = ogr.wkbGeometryCollection
        return result

    # Ouverture du fichier d'entrée
    in_format_file, in_extension = format_gis(entree)
    in_driver = ogr.GetDriverByName(in_format_file)
    in_data_source = in_driver.Open(entree, 0)
    in_layer = in_data_source.GetLayer()
    in_proj_ref = in_layer.GetSpatialRef()

    # Création du fichier de sortie
    out_format_file, out_extension = format_gis(sortie)
    out_driver = ogr.GetDriverByName(out_format_file)
    out_data_source = out_driver.CreateDataSource(sortie)

    # Détermination du type de géométrie pour le fichier de sortie
    if out_extension in {'.CSV', '.DBF'}:
        out_geo_type = ogr.wkbNone
        out_proj_ref = None
    else:
        out_geo_type = geometry_gis(in_layer, in_format_file)
        out_proj_ref = in_proj_ref

    # Gestion de la transformation de projection
    if proj_change and out_geo_type != ogr.wkbNone:
        in_layer_proj = osr.SpatialReference()
        in_layer_proj.ImportFromEPSG(proj_change[0])
        out_layer_proj = osr.SpatialReference()
        out_layer_proj.ImportFromEPSG(proj_change[1])
        out_proj_ref = out_layer_proj

    # Création de la nouvelle couche dans le fichier de sortie
    out_layer = out_data_source.CreateLayer(in_layer.GetName(), out_proj_ref, out_geo_type)

    # Création des champs dans la couche de sortie
    for i in range(in_layer.GetLayerDefn().GetFieldCount()):
        in_field = in_layer.GetLayerDefn().GetFieldDefn(i)
        out_field = ogr.FieldDefn(in_field.GetName(), in_field.GetType())
        out_field.SetWidth(in_field.GetWidth())
        out_field.SetPrecision(in_field.GetPrecision())
        out_layer.CreateField(out_field)

    # Copie des entités de la couche d'entrée vers la couche de sortie
    for in_feature in in_layer:
        out_feature = in_feature.Clone()
        if proj_change and out_geo_type != ogr.wkbNone:
            in_geometry = in_feature.GetGeometryRef()
            in_geometry.AssignSpatialReference(in_layer_proj)
            in_geometry.TransformTo(out_layer_proj)
            out_feature.SetGeometry(in_geometry)
        if out_geo_type == ogr.wkbNone:
            out_feature.SetGeometry(None)
        out_layer.CreateFeature(out_feature)

    out_data_source = None  # Fermeture du fichier de sortie

# Exécution de la fonction
entree = "data/input/Point.shp"
sortie = "data/output/select_point_translate.geojson"
translator_gis(entree=entree, sortie=sortie, proj_change=[2154, 4326])
