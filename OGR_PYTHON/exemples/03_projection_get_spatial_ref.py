from osgeo import ogr

# Chargement du driver pour le format Shapefile
driver = ogr.GetDriverByName("ESRI Shapefile")

# Ouverture du fichier Shapefile en mode lecture seule (0)
data_source = driver.Open("data/Point.shp", 0)

# Récupération de la couche
layer = data_source.GetLayer()

# Affichage de la référence spatiale de la couche
print(f"Les référence spatial de la couche : {layer.GetSpatialRef()}")
