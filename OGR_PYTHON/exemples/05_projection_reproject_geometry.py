from osgeo import ogr
from osgeo import osr

# Chargement du driver pour le format Shapefile
driver = ogr.GetDriverByName("ESRI Shapefile")

# Ouverture du fichier Shapefile en mode lecture seule (0)
data_source = driver.Open("data/Point.shp", 0)

# Récupération de la couche
layer = data_source.GetLayer()

# Création d'une instance de SpatialReference pour stocker la projection
projection = osr.SpatialReference()

# Importation d'un système de projection via un code EPSG.
# On peut importer un système de projection depuis différentes sources :
# WKT, PROJ4, URL, ESRI, EPSG, EPSGA, PCI, USGS, XML, et ERM.
projection.ImportFromEPSG(4326)
print(f"Référence de projection 4326 : {projection}")

# Définition des systèmes de référence pour la transformation de WGS84 (EPSG:4326) vers RGF93 (EPSG:2154)
WGS84 = osr.SpatialReference()
WGS84.ImportFromEPSG(4326)

RGF93 = osr.SpatialReference()
RGF93.ImportFromEPSG(2154)
print(f"Référence de projection 2154 : {RGF93}")
print()
# Coordonnées de Paris en WGS84
lat = 48.8155730
lon = 2.2241990
# Note : en WGS84, les coordonnées sont exprimées en (latitude, longitude), ce qui
# peut prêter à confusion car habituellement on utilise (x, y).

# Création d'une géométrie OGR pour représenter le point de Paris
paris = ogr.CreateGeometryFromWkt(f"POINT ({lon} {lat})")
print(f"Coordonnées de Paris en WGS84 : {paris}")

# Attribution du système de coordonnées WGS84 au point de Paris
paris.AssignSpatialReference(WGS84)

# Transformation des coordonnées du point de WGS84 vers RGF93
paris.TransformTo(RGF93)

# Affichage des coordonnées transformées en RGF93
print(f"Nouvelles coordonnées de Paris en RGF93 : {paris}")
