from osgeo import ogr

# Création d'un driver en fonction du format du fichier (ici SHAPEFILE)
# Liste des noms de formats disponible ici : http://www.gdal.org/ogr/ogr_formats.html
driver = ogr.GetDriverByName("ESRI SHAPEFILE")

# Création d'une source de données pointant vers le Shapefile.
# Premier paramètre (string) : chemin vers la source
# Deuxième paramètre (booléen) : 1 si modifiable, 0 si en lecture seule
data_source = driver.Open("data/Polygon.shp", 0)

# Certains formats contiennent plusieurs couches d'information géographique,
# comme le MIF/MID ou les bases de données telles que PostGIS.
# Pour connaître le nombre de couches présentes, on utilise la fonction suivante :
print(f"Nombre de couches dans le fichier : {data_source.GetLayerCount()}")

# On ouvre la première (et unique) couche (par défaut, la valeur est zéro,
# il n'est donc pas nécessaire de la préciser dans ce cas).
layer = data_source.GetLayer(0)

# À partir de cette étape, le document est ouvert.
# On peut déjà récupérer des informations sur la couche :
print(f"Nom de la couche : {layer.GetName()}")
print(f"Enveloppe de la couche : {layer.GetExtent()}")
print(f"Nombre d'entités : {layer.GetFeatureCount()}")
print(f"Géométrie de la couche : {layer.GetGeomType()}")
print(f"Référence spatiale : {layer.GetSpatialRef()}")

# Pour accéder aux données de la couche, on doit pointer la définition de la couche.
layer_defn = layer.GetLayerDefn()
print(f"Nombre de champs : {layer_defn.GetFieldCount()}")

# Les informations sur les champs sont encapsulées dans la définition de la couche.
# On peut récupérer : le nom des champs, le type, la longueur, et la précision (si de type double).
for i in range(layer_defn.GetFieldCount()):
    print("\n")  # On saute une ligne pour plus de lisibilité
    print(f"Nom du champ {i} : {layer_defn.GetFieldDefn(i).GetName()}")
    print(f"Type du champ {i} : {layer_defn.GetFieldDefn(i).GetType()}")
    print(f"Longueur du champ {i} : {layer_defn.GetFieldDefn(i).GetWidth()}")
    print(f"Précision du champ {i} : {layer_defn.GetFieldDefn(i).GetPrecision()}")

# ACCÈS AUX DONNÉES ATTRIBUTAIRES

# ATTENTION : IL EXISTE DEUX MÉTHODES POUR ACCÉDER AUX DONNÉES

# MÉTHODE N°1
# Pour accéder à une entité par son numéro de ligne (attention à vérifier
# si le format prend en compte ou non les noms de champs comme appartenant à une ligne).
print("MÉTHODE 1 : j'affiche l'entité 1 (2ème ligne)")
feature = layer.GetFeature(1)
for i in range(layer_defn.GetFieldCount()):
    print(f"{layer_defn.GetFieldDefn(i).GetName()} : {feature.GetField(i)}")

print("\nMÉTHODE 2 : j'affiche toute la couche")
# MÉTHODE N°2
# Cette méthode permet de visualiser toutes les entités de la couche.
for feature in layer:
    for j in range(layer_defn.GetFieldCount()):
        print(f"{layer_defn.GetFieldDefn(j).GetName()} : {feature.GetField(j)}")

# ACCÈS À LA GÉOMÉTRIE
# La géométrie est également stockée dans l'objet feature.
# Nous allons examiner la 3ème entité de la couche.
feature = layer.GetFeature(2)
geometry = feature.GetGeometryRef()

# Récupération du type de géométrie
print(f"Type de géométrie : {geometry.GetGeometryName()}")
print(f"Code de géométrie OGR : {geometry.GetGeometryType()}")

# Puisque l'on travaille sur une couche de type polygone, on peut récupérer :
# - l'aire
print(f"Aire du polygone : {geometry.GetArea()}")
# - l'enveloppe
print(f"Enveloppe du polygone : {geometry.GetEnvelope()}")

# Les coordonnées en différents formats :
# - WKT (Well-Known Text)
print(f"Coordonnées en WKT : {geometry.ExportToWkt()}")
# - WKB (Well-Known Binary)
print(f"Coordonnées en WKB : {geometry.ExportToWkb()}")
# - GEOJSON
print(f"Coordonnées en GeoJSON : {geometry.ExportToJson()}")
