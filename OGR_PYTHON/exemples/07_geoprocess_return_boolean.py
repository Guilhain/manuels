from osgeo import ogr

# Ouverture du fichier de points
point_driver = ogr.GetDriverByName("ESRI Shapefile")
point_data_source = point_driver.Open("data/Point.shp", 0)
point_layer = point_data_source.GetLayer()

# Ouverture du fichier de lignes
ligne_driver = ogr.GetDriverByName("ESRI Shapefile")
ligne_data_source = ligne_driver.Open("data/Ligne.shp", 0)
ligne_layer = ligne_data_source.GetLayer()

# Ouverture du fichier de polygones
polygone_driver = ogr.GetDriverByName("ESRI Shapefile")
polygone_data_source = polygone_driver.Open("data/Polygon.shp", 0)
polygone_layer = polygone_data_source.GetLayer()

# Recherche des lignes qui intersectent le premier polygone
# Récupération de la géométrie du premier polygone
polygone_feature = polygone_layer.GetFeature(0)
polygone_geometry = polygone_feature.GetGeometryRef()

# Boucle sur toutes les entités de la couche de lignes
for i in range(ligne_layer.GetFeatureCount()):
    ligne_feature = ligne_layer.GetFeature(i)
    ligne_geometry = ligne_feature.GetGeometryRef()

    # Vérification de l'intersection entre la ligne et le polygone
    if ligne_geometry.Intersect(polygone_geometry):
        print(f"{ligne_feature.GetField('NOM')} intersecte {polygone_feature.GetField('NOM')}")

        # Vérification si la ligne est totalement contenue dans le polygone
        if polygone_geometry.Contains(ligne_geometry):
            print(f"{ligne_feature.GetField('NOM')} est totalement compris dans {polygone_feature.GetField('NOM')}")

# Vérification si la troisième ligne croise un des polygones
ligne_feature = ligne_layer.GetFeature(2)
ligne_geometry = ligne_feature.GetGeometryRef()

# Boucle sur toutes les entités de la couche de polygones
for i in range(polygone_layer.GetFeatureCount()):
    polygone_feature = polygone_layer.GetFeature(i)
    polygone_geometry = polygone_feature.GetGeometryRef()

    # Vérification si la ligne croise le polygone
    if ligne_geometry.Crosses(polygone_geometry):
        print(f"{ligne_feature.GetField('NOM')} croise le {polygone_feature.GetField('NOM')}")

# Recherche des points situés à une distance de 0,2 de la deuxième ligne
ligne_feature = ligne_layer.GetFeature(1)
ligne_geometry = ligne_feature.GetGeometryRef()

# Création d'un buffer autour de la ligne (qui devient un polygone représentant la zone autour de la ligne)
ligne_buffer = ligne_geometry.Buffer(0.2)
print(f"Type de géométrie : {ligne_buffer.GetGeometryType()}")

# Boucle sur toutes les entités de la couche de points
for i in range(point_layer.GetFeatureCount()):
    point_feature = point_layer.GetFeature(i)
    point_geometry = point_feature.GetGeometryRef()

    # Vérification si le point est situé à une distance inférieure à 0,2 de la ligne
    if point_geometry.Intersect(ligne_buffer):
        print(f"{point_feature.GetField('NOM')} est situé à 0,2 de distance de {ligne_feature.GetField('NOM')}")
