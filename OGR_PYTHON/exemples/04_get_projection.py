from osgeo import ogr
from osgeo import osr

# Création d'une variable de type SpatialReference pour stocker les références
# de projection géographique.
projection = osr.SpatialReference()

# Importation d'un système de projection en utilisant un code EPSG.
# On peut importer un système de projection via plusieurs formats ou sources,
# notamment WKT, PROJ4, URL, ESRI, EPSG, EPSGA, PCI, USGS, XML, et ERM.
projection.ImportFromEPSG(4326)

# Affichage de la projection pour vérification
print(projection)
