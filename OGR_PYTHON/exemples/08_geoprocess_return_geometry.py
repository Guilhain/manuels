from osgeo import ogr

# Ouverture du fichier de points
point_driver = ogr.GetDriverByName("ESRI Shapefile")
point_data_source = point_driver.Open("data/Point.shp", 0)
point_layer = point_data_source.GetLayer()

# Ouverture du fichier de lignes
ligne_driver = ogr.GetDriverByName("ESRI Shapefile")
ligne_data_source = ligne_driver.Open("data/Ligne.shp", 0)
ligne_layer = ligne_data_source.GetLayer()

# Recherche des points situés à une distance de 0,2 de la ligne 2
ligne_feature = ligne_layer.GetFeature(1)
ligne_geometry = ligne_feature.GetGeometryRef()

# Création d'un buffer autour de la ligne (transformant la ligne en polygone représentant la zone autour d'elle)
ligne_buffer = ligne_geometry.Buffer(0.2)
print(f"Type de géométrie : {ligne_buffer.GetGeometryType()}")

# Boucle sur toutes les entités de la couche de points
for i in range(point_layer.GetFeatureCount()):
    point_feature = point_layer.GetFeature(i)
    point_geometry = point_feature.GetGeometryRef()

    # Vérification si le point est situé à une distance inférieure à 0,2 de la ligne
    if point_geometry.Intersect(ligne_buffer):
        print(f"{point_feature.GetField('NOM')} est situé à 0,2 de distance de {ligne_feature.GetField('NOM')}")
