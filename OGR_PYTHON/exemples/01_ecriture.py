from osgeo import ogr

## CREATION DE LA COUVERTURE
# Création d'un driver de connexion pour PostgreSQL
driver = ogr.GetDriverByName("POSTGRESQL")
# Définition du chemin de connexion pour la base de données PostgreSQL
data_source = driver.CreateDataSource(
    "PG: host='localhost' dbname='carthageo' user='postgres' password='postgres' port=5434"
)

# Création de la couche avec vérification préalable de son existence
if data_source.GetLayerByName("nouvelle_couche"):
    raise Exception(
        "La couche existe déjà, veuillez la supprimer dans la base avant de relancer le script."
    )
layer = data_source.CreateLayer("nouvelle_couche", None, 0)

## ECRITURE DES CHAMPS
# Création de champs dans la couche pour distinguer les différentes géométries :
# un identifiant (le code de géométrie), entier de longueur 3
# un nom (string) de longueur 20
# une aire (réel) de longueur 15 et précision 5
# un périmètre (réel) de longueur 15 et précision 5
field_name = ["ID", "NOM", "AIRE", "PERIMETRE"]
field_type = [0, 4, 2, 2]
field_width = [3, 20, 15, 15]
field_precision = [0, 0, 5, 5]

for i in range(len(field_name)):
    # Création de chaque champ avec son nom, type, longueur et précision
    field = ogr.FieldDefn(field_name[i], field_type[i])
    field.SetWidth(field_width[i])
    field.SetPrecision(field_precision[i])
    layer.CreateField(field)

## ECRITURE DES DONNEES ET DE LA GEOMETRIE
# Définition des géométries en WKT (Well-Known Text)
point = "POINT (30 10)"
ligne = "LINESTRING (30 10, 10 30, 40 40)"
polygone = "POLYGON ((35 10, 10 20, 15 40, 45 45, 35 10),(20 30, 35 35, 30 20, 20 30))"
multi_point = "MULTIPOINT ((10 40), (40 30), (20 20), (30 10))"
multi_ligne = "MULTILINESTRING ((10 10, 20 20, 10 40),(40 40, 30 30, 40 20, 30 10))"
multi_polygone = "MULTIPOLYGON (((40 40, 20 45, 45 30, 40 40)),((20 35, 45 20, 30 5, 10 10, 10 30, 20 35),(30 20, 20 25, 20 15, 30 20)))"

# Ajout de chaque géométrie individuellement, avec les informations spécifiques
# Il se peut que certains "warning d'OGR apparaissent sur le terminal python

# POINT
feature = ogr.Feature(layer.GetLayerDefn())
geometry = ogr.CreateGeometryFromWkt(point)
feature.SetGeometry(geometry)
feature.SetField("ID", 1)
feature.SetField("NOM", "Point")
feature.SetField("AIRE", geometry.Area())
feature.SetField("PERIMETRE", geometry.Length())
layer.CreateFeature(feature)

# LIGNE
feature = ogr.Feature(layer.GetLayerDefn())
geometry = ogr.CreateGeometryFromWkt(ligne)
feature.SetGeometry(geometry)
feature.SetField("ID", 2)
feature.SetField("NOM", "Ligne")
feature.SetField("AIRE", geometry.Area())
feature.SetField("PERIMETRE", geometry.Length())
layer.CreateFeature(feature)

# POLYGONE
feature = ogr.Feature(layer.GetLayerDefn())
geometry = ogr.CreateGeometryFromWkt(polygone)
feature.SetGeometry(geometry)
feature.SetField("ID", 3)
feature.SetField("NOM", "Polygone")
feature.SetField("AIRE", geometry.Area())
# Pour le champ périmètre sur les polygones, on doit extraire le contour
# puis calculer sa longueur
feature.SetField("PERIMETRE", geometry.Boundary().Length())
layer.CreateFeature(feature)

# MULTIPOINT
feature = ogr.Feature(layer.GetLayerDefn())
geometry = ogr.CreateGeometryFromWkt(multi_point)
feature.SetGeometry(geometry)
feature.SetField("ID", 4)
feature.SetField("NOM", "MultiPoint")
feature.SetField("AIRE", geometry.Area())
feature.SetField("PERIMETRE", geometry.Length())
layer.CreateFeature(feature)

# MULTILIGNE
feature = ogr.Feature(layer.GetLayerDefn())
geometry = ogr.CreateGeometryFromWkt(multi_ligne)
feature.SetGeometry(geometry)
feature.SetField("ID", 5)
feature.SetField("NOM", "MultiLigne")
feature.SetField("AIRE", geometry.Area())
feature.SetField("PERIMETRE", geometry.Length())
layer.CreateFeature(feature)

# MULTIPOLYGONE
feature = ogr.Feature(layer.GetLayerDefn())
geometry = ogr.CreateGeometryFromWkt(multi_polygone)
feature.SetGeometry(geometry)
feature.SetField("ID", 6)
feature.SetField("NOM", "MultiPolygon")
feature.SetField("AIRE", geometry.Area())
# Pour le champ périmètre sur les polygones, on doit extraire le contour
# puis calculer sa longueur
feature.SetField("PERIMETRE", geometry.Boundary().Length())
layer.CreateFeature(feature)
