from osgeo import ogr

# Chemin du fichier Shapefile
fichier = "data/countries/countries.shp"

# Chargement du driver pour le format Shapefile
driver = ogr.GetDriverByName("ESRI Shapefile")

# Ouverture du fichier Shapefile
data_source = driver.Open(fichier)
layer = data_source.GetLayer()

# Application d'un filtre attributaire pour sélectionner les pays dont le champ
# "continent" est défini comme étant "Africa"
layer.SetAttributeFilter("continent = 'Africa'")

# Affichage du nom des pays dans le résultat de la requête
# Le nom du pays est stocké dans le champ "sovereignt"
for feature in layer:
    print(feature.GetField("sovereignt"))
