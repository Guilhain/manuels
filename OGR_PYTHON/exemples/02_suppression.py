from osgeo import ogr

fichier = "data/del/Point.shp"
driver = ogr.GetDriverByName("ESRI Shapefile")

# Suppression du fichier Shapefile :
# Attention, cette opération ne peut être exécutée qu'une seule fois
# (le fichier doit exister la première fois que cette commande est exécutée).
driver.DeleteDataSource(fichier)
