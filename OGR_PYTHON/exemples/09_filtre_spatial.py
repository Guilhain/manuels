from osgeo import ogr

# Chemin du fichier Shapefile
fichier = "data/countries/countries.shp"

# Chargement du driver pour le format Shapefile
driver = ogr.GetDriverByName("ESRI Shapefile")

# Ouverture du fichier Shapefile
data_source = driver.Open(fichier)
layer = data_source.GetLayer()

# Application d'une sélection spatiale pour filtrer les pays situés dans le quart Sud-Est du monde
wkt_se = "POLYGON ((0 0, 180 0, 180 -90, 0 -90, 0 0))"

# Création de la géométrie OGR pour le polygone de la zone Sud-Est
geometry = ogr.CreateGeometryFromWkt(wkt_se)
layer.SetSpatialFilter(geometry)

# Affichage du nom des pays dans le résultat de la requête
for feature in layer:
    print(f"{feature.GetField('sovereignt')}")
