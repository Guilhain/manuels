<!-- TOC -->
* [OGRINFO](#ogrinfo)
  * [Compter le nombre de lignes dans une table](#compter-le-nombre-de-lignes-dans-une-table)
* [OGR2OGR](#ogr2ogr)
  * [Faire une requête sur un fichier (base de données et hors base de données)](#faire-une-requête-sur-un-fichier-base-de-données-et-hors-base-de-données)
  * [Changer le format de sortie d'un fichier](#changer-le-format-de-sortie-dun-fichier)
  * [Changer la projection](#changer-la-projection)
  * [Intégrer une requête SQL](#intégrer-une-requête-sql)
  * [Importation de données géographiques dans une BDD PostGIS](#importation-de-données-géographiques-dans-une-bdd-postgis)
  * [Concaténation de Shapefiles](#concaténation-de-shapefiles)
  * [Importation d'un fichier DBF dans une BDD PostGIS](#importation-dun-fichier-dbf-dans-une-bdd-postgis)
  * [Problèmes liés aux importations de texte](#problèmes-liés-aux-importations-de-texte)
<!-- TOC -->

# OGRINFO

OGRINFO permet de faire des requêtes sur une base de données ou un fichier sans avoir à exporter ou importer un fichier, contrairement à OGR2OGR.
OGRINFO sert principalement pour la consultation de données, tandis que OGR2OGR est utile pour la consultation, mais aussi pour l'édition et la création de données.

## Compter le nombre de lignes dans une table :

```shell
ogrinfo -sql "SELECT COUNT(*) FROM [NOM DE LA TABLE] PG:"hostaddr=[adresse du serveur, si localhost alors : 127.0.0.1] dbname=[Nom de la BDD] user=[Nom de l'utilisateur] password=[mot de passe]"
```

> ogrinfo -sql "SELECT COUNT(*) FROM grille" PG:"hostaddr=127.0.0.1 dbname=ESPON user=postgres password=paris"

# OGR2OGR

## Faire une requête sur un fichier (base de données et hors base de données)

``` shell
ogr2ogr [Chemin du fichier en sortie (avec son nom et son extension)] -sql "[Requête SQL sur champ de la table attributaire (exemple : select * from le_fichier_shape_d'origine where "CHAMP1"='XX' order by "CHAMP2")]" [Chemin du fichier d'entrée (avec son nom et son extension)]
```

## Changer le format de sortie d'un fichier

``` shell
ogr2ogr -f "[Format de sortie voulu]" [Chemin du fichier en sortie (avec son nom et son extension)] [Chemin du fichier d'entrée (avec son nom et son extension)]
```

## Changer la projection

``` shell
ogr2ogr -t_srs "EPSG:[Numéro EPSG de la projection souhaitée]" -s_srs "EPSG:[Numéro EPSG de la projection native]" [Chemin du fichier d'entrée (avec son nom et son extension)]
```
> ogr2ogr -t_srs "EPSG:2256" -s_srs "EPSG:4326" -f "ESRI Shapefile" C:\MAGALI\SHP\Mairies_FR_OGR2OGRV3.shp C:\MAGALI\MI\Mairies_FR_WGS84.tab

## Intégrer une requête SQL

``` shell
ogr2ogr -f "[Format de sortie voulu]" [Chemin du fichier en sortie (avec son nom et son extension)] -sql "[Requête SQL sur champ de la table attributaire (exemple : select * from le_fichier_shape_d'origine where "CHAMP1"='XX' order by "CHAMP2")]" [Chemin du fichier d'entrée (avec son nom et son extension)]
```

## Importation de données géographiques dans une BDD PostGIS

``` shell
ogr2ogr -f "[Format de sortie voulu]" PG:"hostaddr=[adresse du serveur, si localhost alors : 127.0.0.1] dbname=[Nom de la BDD] user=[Nom de l'utilisateur] password=[mot de passe]" [Chemin du fichier d'entrée (avec son nom et son extension)] -lco GEOMETRY_NAME=the_geom
```
> ogr2ogr -f "PostgreSQL" -overwrite PG:"hostaddr=127.0.0.1 dbname=ESPON user=postgres password=paris" G:\TEMP\UMZ_Project.shp -lco GEOMETRY_NAME=the_geom

## Concaténation de Shapefiles

``` shell
ogr2ogr -append -update [Chemin du fichier en sortie (avec son nom et son extension)] [Chemin du fichier d'entrée (avec son nom et son extension)] -nln [Nom du fichier en sortie (sans extension)]
```

## Importation d'un fichier DBF dans une BDD PostGIS

```shell
ogr2ogr -f "[Format de sortie voulu]" PG:"hostaddr=[adresse du serveur, si localhost alors : 127.0.0.1] dbname=[Nom de la BDD] user=[Nom de l'utilisateur] password=[mot de passe]" [Chemin du fichier d'entrée (avec son nom et son extension)] -nln "[Nom de la table dans la BDD]" -lco PRECISION=NO
```
> ogr2ogr -f "PostgreSQL" PG:"host=myserver user=myusername dbname=mydbname password=mypassword" sometable.dbf -nln "sometable"

## Problèmes liés aux importations de texte

Insérer dans la ligne avant la requête OGR2OGR l'instruction suivante :

```shell
SET PGCLIENTENCODING=8859-1
SET PGCLIENTENCODING=LATIN1
```